import { Injectable, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Coordinates } from '../models/coordinates';
import { Observable, Observer } from 'rxjs';
import { Order } from '../models/order';
import { NGXLogger } from 'ngx-logger';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})

export class LocationService implements OnInit {

  public coordinates: Coordinates = new Coordinates();
  private coordinateSource = new BehaviorSubject(this.coordinates);
  public currentCoords = this.coordinateSource.asObservable();

  private navigation: Coordinates = new Coordinates();
  private navigationSource = new BehaviorSubject(this.navigation);
  public navigationStatus = this.navigationSource.asObservable();

  constructor(private logger: NGXLogger, private router: Router) { }

  ngOnInit() {
    this.setCurrentPosition();
  }

  calculateDistance(coordinates: Coordinates): number {
    let distance = Math.sqrt(Math.pow(coordinates.fromLat - coordinates.whereLat, 2) + (Math.pow(coordinates.fromLong - coordinates.whereLong, 2))) * 100;
    this.logger.debug(`LocationService: Calculated distance: ${distance} based on coordinates ${JSON.stringify(coordinates)}`);
    this.updateWhereCoordinatesWithName(coordinates);
    return distance;
  }

  changeCoordinates(coords: {
    fromLat: number,
    fromLong: number,
    whereLat: number,
    whereLong: number,
    fromName: string,
    whereName: string
  }) {
    this.coordinateSource.next(coords);
    this.logger.debug(`LocationService: Changed coordinates variable from ${JSON.stringify(this.coordinates)} to ${JSON.stringify(coords)}`);
  }

  resetDestinationCoordinates() {
    this.coordinates.whereLat = null;
    this.coordinates.whereLong = null;
    this.setCurrentPosition();
    this.coordinateSource.next(this.coordinates);
    this.logger.debug(`LocationService: Reset where coordinates`);
  }

  updateFromCoordinatesWithName(input: string) {
    let name = input.split(",")[0];
    let coords = this.coordinates;
    coords.fromName = name;
    this.coordinateSource.next(coords);
    this.logger.debug(`LocationService: Changed coordinates name from ${JSON.stringify(this.coordinates.fromName)} to ${JSON.stringify(name)}`);
  }

  updateWhereCoordinatesWithName(coordinates: Coordinates) {
    if (coordinates.whereLat && coordinates.whereLong) {
      this.geocode(coordinates.whereLat, coordinates.whereLong).subscribe(result => {
        this.logger.debug(`LocationService: Reverse geocoding address from coordinates: ${result[0].formatted_address}`);
        coordinates.whereName = result[0].formatted_address.split(",")[0];
        this.coordinateSource.next(coordinates);
        this.coordinates = coordinates;
        this.logger.debug(`LocationService: Changed navigation coordinates name from ${JSON.stringify(this.navigation)} to ${JSON.stringify(coordinates)}`);
      }, error => {
        this.logger.error(`LocationService: Error while reverse geocoding address from coordinates: ${error}`);
        this.router.navigate(['']);

      });
      let coords = this.coordinates;
      coords.whereName = name;
      this.coordinateSource.next(coords);
      this.logger.debug(`LocationService: Changed coordinates name from ${JSON.stringify(this.coordinates.whereName)} to ${JSON.stringify(name)}`);
    }
  }

  setNavigationCoordinates(order: Order) {
    if (order === null || order.status === "CANCELLED" || order.status === "ACCEPTED" || order.status === "PAID" || order.status === "TERMINATED") {
      this.navigationSource.next(null);
      this.logger.debug(`LocationService: Order doesn't exist or is inactive set navigation coordinates to null`);
    } else {
      let coordinates = this.getCoordinatesFromOrder(order);
      this.navigationSource.next(coordinates);
      this.navigation = coordinates;
      this.logger.debug(`LocationService: Order exists and is active, set navigation coordinates to ${JSON.stringify(this.navigation)}`);
    }
  }

  getCoordinatesFromOrder(order: Order): Coordinates {
    let coordinates: Coordinates = new Coordinates();
    coordinates.fromLat = order.fromLat;
    coordinates.fromLong = order.fromLong;
    coordinates.whereLat = order.whereLat;
    coordinates.whereLong = order.whereLong;
    coordinates.fromName = order.fromName;
    coordinates.whereName = order.whereName;
    this.logger.debug(`LocationService: Fetched coordinates ${coordinates} from the order ${JSON.stringify(order)}`);
    return coordinates;
  }

  geocode(lat: number, lng: number): Observable<google.maps.GeocoderResult[]> {
    return Observable.create((observer: Observer<google.maps.GeocoderResult[]>) => {
      // Invokes geocode method of Google Maps API geocoding.
      let latlng = new google.maps.LatLng(lat, lng);
      let geocoder = new google.maps.Geocoder();
      geocoder.geocode({ location: latlng }, (
        (results: google.maps.GeocoderResult[], status: google.maps.GeocoderStatus) => {
          if (status === google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
            setTimeout(function () {
            }, 1000);
          }
          else if (status === google.maps.GeocoderStatus.OK) {
            observer.next(results);
            observer.complete();
          } else {
            this.logger.error('LocationService: Geocoding service: geocoder failed due to: ' + status);
            observer.error(status);
          }
        })
      );
    });
  }

  setCurrentPosition(): Promise<any> {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(resp => {
        this.coordinates.fromLong = resp.coords.longitude;
        this.coordinates.fromLat = resp.coords.latitude;
        this.coordinateSource.next(this.coordinates);
        this.geocode(resp.coords.latitude, resp.coords.longitude).subscribe(result => {
          this.logger.debug(`LocationService: Reverse geocoding address from coordinates: ${result[0].formatted_address}`);
          this.updateFromCoordinatesWithName(result[0].formatted_address)
        });
        this.logger.debug(`LocationService: Set current position to ${JSON.stringify(this.coordinates)}`)
        resolve({
          lng: resp.coords.longitude,
          lat: resp.coords.latitude
        });
      },
        err => {
          reject(err);
          this.logger.error(`LocationService: Error getting current position`);
        });
    });
  }
}