import { StompService, StompConfig, StompState } from "@stomp/ng2-stompjs";
import { Message } from "@stomp/stompjs";
import { Observable, BehaviorSubject } from "rxjs";

export class WebSocketService {
    private messages: Observable<Message>;
    private stompService: StompService;

    constructor() {
    }

    public init(socketUrl: string, streamUrl: string) {
        let stompConfig: StompConfig = {
            url: socketUrl,
            headers: {
                login: "",
                passcode: ""
            },
            heartbeat_in: 0,
            heartbeat_out: 20000,
            reconnect_delay: 5000,
            debug: true
        };

        // Create Stomp Service
        this.stompService = new StompService(stompConfig);

        // Connect to a Stream
        this.messages = this.stompService.subscribe(streamUrl);
    }

    public stream(): Observable<Message> {
        return this.messages;
    }

    public send(url: string, message: any) {
        return this.stompService.publish(url, JSON.stringify(message));
    }

    public state(): BehaviorSubject<StompState> {
        return this.stompService.state;
    }
}