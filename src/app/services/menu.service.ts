import { Injectable } from '@angular/core';
import { OrderService } from './order.service';
import { Order } from '../models/order'
import { Orders } from '../models/orders'
import { User } from '../models/user'
import { Link } from "../models/link";
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs';
import { WebSocketService } from './web-socket.service';
import { Message } from "@stomp/stompjs";
import { StompState } from "@stomp/ng2-stompjs";
import { MessageService } from './message.service';
import { LocationService } from './location.service';
import { NGXLogger } from 'ngx-logger';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  private currentUser: User;
  private url: string;
  public lastOrder: Order;
  private state: string = "NOT CONNECTED";
  private links: Link = new Link();
  private linksSource = new BehaviorSubject(this.links);
  public linksStatus = this.linksSource.asObservable();

  constructor(private orderService: OrderService,
    private webSocketService: WebSocketService,
    private messageService: MessageService,
    private locationService: LocationService,
    private logger: NGXLogger,
    private router: Router
  ) { }

  monitorBroadcastMsg() {
    // Subscribe to its stream (to listen on messages)
    this.webSocketService.stream().subscribe((message: Message) => {
      if (message.body !== "\"null\"" && message.body !== "") {
        let response = JSON.parse(message.body);
        let responseOrder: Order = <Order>response[0];
        let action = response[1];
        this.logger.debug(`MenuService: Received broadcased message: ${JSON.stringify(message.body)}`);
        if (this.checkIfMessageRequireActions(responseOrder, action)) {
          this.orderService.getOrderById(responseOrder.orderId).subscribe(order => {
            this.updateLinks(order);
            // this.locationService.setNavigationCoordinates(order); // is it needed?
            if (action === "terminate" || action === "pay" || action === "summary") {
              this.generateSummaryPopup(order);
            } else {
              let type = "info";
              if (action === "accept") {
                type = action;
              }
              this.messageService.showInfoPopup(order, action, type);
              this.orderService.setLastOrder(order);
            }
          }, error => {
            this.logger.error(`MenuService: Error while getting order by id: ${JSON.stringify(error)}`);
            this.router.navigate(['']);
          });
        }
      }
    });
    // Subscribe to its state (to know its connected or not)
    this.webSocketService.state().subscribe((state: StompState) => {
      this.state = StompState[state];
    });
  }

  generateSummaryPopup(order: Order) {
    if (this.currentUser.authorities[0].authority === "ROLE_PASSENGER" && order.status === "PAID") {
      this.messageService.showSummaryPopupWithRating(order).subscribe(rating => {
        this.orderService.addRating(rating, order).subscribe(response => {
          this.logger.debug(`MenuService: Added rating ${rating} to the order ${order.orderId}`);
          this.webSocketService.send("/server-receiver", [response, "summary"]);
        }, error => {
          this.logger.error(`MenuService: Couldnt add a rating ${rating} to the order ${order.orderId} due to ${error}`);
          this.router.navigate(['']);
        })
      }), error => {
        this.logger.error(`MenuService: Error while receiving ride rating: ${JSON.stringify(error)}`);
        this.router.navigate(['']);
      };
    } else {
      this.messageService.showSummaryPopup(order);
    }
    this.locationService.resetDestinationCoordinates();
  }


  checkIfMessageRequireActions(responseOrder: Order, action: string): boolean {
    let status: boolean;
    if (action === "create" || (action === "cancel" && responseOrder.driver === null)) {
      status = false;
    } else if (action === "summary" && this.currentUser.authorities[0].authority === 'ROLE_DRIVER' && responseOrder.rating !== null) {
      status = true;
    } else if (action === "summary" && this.currentUser.authorities[0].authority === 'ROLE_PASSENGER' && responseOrder.rating !== null) {
      status = false;
    } else {
      if (this.currentUser.authorities[0].authority === 'ROLE_PASSENGER') {
        if (this.currentUser.username === responseOrder.passenger.username) {
          this.logger.debug(`MenuService: Received order belongs to the user and require action: ${action}`);
          status = this.compareOrderWithLastOneRecorded(responseOrder);
        }
      } else if (this.currentUser.authorities[0].authority === 'ROLE_DRIVER') {
        if (this.currentUser.username === responseOrder.driver.username) {
          this.logger.debug(`MenuService: Received order belongs to the user and require action: ${action}`);
          status = this.compareOrderWithLastOneRecorded(responseOrder);
        }
      }
    }
    return status;
  }


  compareOrderWithLastOneRecorded(responseOrder): boolean {
    let status: boolean = false;
    this.logger.debug(`MenuService: Comparing with the last order: ${JSON.stringify(this.lastOrder)} with received order ${JSON.stringify(responseOrder)}`)
    if (this.lastOrder && responseOrder) {
      if (responseOrder.orderId === this.lastOrder.orderId) {
        if (JSON.stringify(this.lastOrder) !== JSON.stringify(responseOrder)) {
          this.logger.debug(`MenuService: Received order and last orders have same id but not equal`);
          status = true;
        }
      }
    }
    return status;
  }

  updateMenu() {
    this.orderService.getActiveOrder().subscribe(activeOrder => {
      this.logger.debug(`MenuService: Received active order: ${JSON.stringify(activeOrder)}`);
      this.updateLinks(activeOrder)
    }, error => {
      this.logger.error(`MenuService: Can't receive an active order: ${JSON.stringify(error)}`);
      this.router.navigate(['']);
    });
  }

  setCurrentUser(user: User) {
    this.currentUser = user;
    this.logger.debug(`MenuService: Set current user to ${JSON.stringify(user)}`);
  }

  updateLinks(order: Order) {
    let linksFromLastOrder: Link = new Link();
    this.logger.debug(`MenuService: Updating links from order: ${JSON.stringify(linksFromLastOrder)}`);

    if (order === null || (!order._links.accept && !order._links.cancel && !order._links.complete && !order._links.start && !order._links.create && !order._links.payment && !order._links.terminate)) {
      this.logger.debug(`MenuService: Order is null or all order links are empty`);
      if (this.currentUser.authorities[0].authority === 'ROLE_DRIVER') {
        this.logger.debug(`MenuService: User is a driver, setting getOpen to href`);
        linksFromLastOrder.getOpen = { href: "getOpen" }
      } else if (this.currentUser.authorities[0].authority === 'ROLE_PASSENGER') {
        this.logger.debug(`MenuService: User is a passenger, setting add to href`);
        linksFromLastOrder.create = { href: "add" }
      }
    } else {
      linksFromLastOrder = order._links;
      this.logger.debug(`MenuService: Order is not null or there are no empty links`);
    }
    this.linksSource.next(linksFromLastOrder);
    this.logger.debug(`MenuService: Updated linksSource with new links`);
    this.orderService.setLastOrder(order);
    this.links = linksFromLastOrder; // not sure why it doesnt work without it TODO
  }

  getActionUrl(action: string): string {
    switch (action) {
      case 'cancel':
        this.url = this.links.cancel.href;
        break;
      case 'complete':
        this.url = this.links.complete.href;
        break;
      case 'payment':
        this.url = this.links.payment.href;
        break;
      case 'accept':
        this.url = this.links.accept.href;
        break;
      case 'start':
        this.url = this.links.start.href;
        break;
      case 'terminate':
        this.url = this.links.terminate.href;
        break;
    }
    let actionUrl = this.url.split('/')[4] + '/' + this.url.split('/')[5];
    this.logger.debug(`MenuService: Got action url: ${actionUrl} from action: ${action}`);
    return actionUrl;
  }

  getListOfOpenOrders(): Observable<Orders> {
    let listOfOrders = this.orderService.getOpenOrders();
    this.logger.debug(`MenuService: Got list of open orders: ${JSON.stringify(listOfOrders)}`);
    return listOfOrders;
  }

  executeAction(action: string) {

    this.logger.debug(`MenuService: Execution action: ${action}`);
    if (action === 'create') {
      this.orderService.add().subscribe(result => {
        this.logger.debug(`MenuService: Added order: ${JSON.stringify(result)}`);
        this.orderService.setLastOrder(result);
        this.updateLinks(result);
        this.webSocketService.send("/server-receiver", [result, action]);
      }, error => {
        this.logger.error(`Could not add order ${error}`);
        this.router.navigate(['']);
      }
      );

    }
    else {
      const url = this.getActionUrl(action);
      this.orderService.execute(url).subscribe(result => {
        this.logger.debug(`MenuService: Executed url: ${url}, got order: ${JSON.stringify(result)}`);
        if (action === 'start') {
          this.locationService.setNavigationCoordinates(result);
        } else {
          this.locationService.setNavigationCoordinates(null);
        }
        this.orderService.setLastOrder(result);
        this.webSocketService.send("/server-receiver", [result, action]);
        this.updateLinks(result);
        if (action === 'cancel' || action === 'payment' || action === 'terminate') {
          this.locationService.resetDestinationCoordinates();
          this.generateSummaryPopup(this.lastOrder);
          this.orderService.setLastOrder(null);
          this.logger.debug(`MenuService: Reset destination coordinates`);
        }
      },
        error => {
          this.logger.error(`MenuService: Error while action execution: ${error}`);
          this.router.navigate(['']);
        });
    }

  }
}