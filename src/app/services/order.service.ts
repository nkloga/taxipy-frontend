import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Order } from "../models/order";
import { Orders } from "../models/orders";
import { map } from 'rxjs/operators';
import { LocationService } from './location.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { NGXLogger } from 'ngx-logger';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';


const RATE_PER_KM = 0.5;
const RATE_RIDE_START = 2;

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  private fixRate: number = 5;
  private baseUrl = `${environment.url}/orders`;
  private newOrder: Order = new Order();
  private lastOrder: Order = new Order();
  private orderSource = new BehaviorSubject(this.lastOrder);
  public orderStatus = this.orderSource.asObservable();
  private messageHistory = [];
  private state: string = "NOT CONNECTED";

  constructor(private http: HttpClient, private logger: NGXLogger, private locationService: LocationService, private router: Router) { }

  add(): Observable<Order> {
    this.locationService.setCurrentPosition();
    this.newOrder.fromLat = this.locationService.coordinates.fromLat;
    this.newOrder.fromLong = this.locationService.coordinates.fromLong;
    this.newOrder.whereLat = this.locationService.coordinates.whereLat;
    this.newOrder.whereLong = this.locationService.coordinates.whereLong;
    this.newOrder.fromName = this.locationService.coordinates.fromName;
    this.newOrder.whereName = this.locationService.coordinates.whereName;
    this.newOrder.distance = this.locationService.calculateDistance(this.locationService.coordinates);
    this.newOrder.cost = this.calculateCost(this.newOrder.distance);
    this.logger.debug(`OrderService: POST new order ${JSON.stringify(this.newOrder)}`);
    return this.http.post(`${this.baseUrl}/add`, this.newOrder)
      .pipe(map((data => <Order>data)));
  }

  calculateCost(distance): number {
    let cost = RATE_RIDE_START + RATE_PER_KM * distance * 2;
    this.logger.debug(`OrderService: set last cost to: ${cost}`);
    return cost;
  }

  setLastOrder(order: Order) {
    this.logger.debug(`OrderService: set last order to: ${JSON.stringify(order)}`);
    this.orderSource.next(order);
  }

  addRating(rating: number, order: Order): Observable<any> {
    return this.http.post(`${this.baseUrl}/${order.orderId}/feedback`, rating)
      .pipe(map((data => <Order>data)));
  }

  getOpenOrders(): Observable<Orders> {
    this.logger.debug(`OrderService: requesting open orders`);
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.get(`${this.baseUrl}/open`, { headers: headers }).pipe(map(data => <Orders>data));
  }

  getOrdersHistory(): Observable<Orders> {
    this.logger.debug(`OrderService: requesting orders history`);
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.get(`${this.baseUrl}/history`, { headers: headers }).pipe(map(data => <Orders>data));
  }

  getActiveOrder(): Observable<Order> {
    this.logger.debug(`OrderService: requesting active order for the user`);
    let order = this.http.get(`${this.baseUrl}/last-open`).pipe(map((data => <Order>data)));
    order.subscribe(order => {
      this.logger.debug(`OrderService: received active order: ${JSON.stringify(order)}, updating orderSource`);
      this.orderSource.next(order);
    }, error => {
      this.logger.debug(`OrderService: error while receiving an active order: ${JSON.stringify(error)}`);
      this.router.navigate(['']);

    });
    return order;
  }

  getOrderById(id: number): Observable<Order> {
    this.logger.debug(`OrderService: requesting open by id ${id}`);
    return this.http.get(`${this.baseUrl}/${id}`)
      .pipe(map((data => <Order>data)))
  }

  execute(action: string): Observable<Order> {
    this.logger.debug(`OrderService: executing action ${action}`);
    return this.http.get(`${this.baseUrl}/${action}`)
      .pipe(map((data => <Order>data)))
  }
}