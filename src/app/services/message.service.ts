import { Injectable } from '@angular/core';
import { Order } from '../models/order';
import { MessageComponent } from '../message/message.component';
import { MatDialog, MatDialogRef } from '@angular/material';
import { NGXLogger } from 'ngx-logger';
import { Observable } from 'rxjs';
import { Orders } from '../models/orders';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  fileNameDialogRef: MatDialogRef<MessageComponent>;
  public rating = ["TERRIBLE", "DISAPPOINTMENT", "OK", "GOOD", "PERFECT"];

  constructor(private dialog: MatDialog, private logger: NGXLogger) { }

  showAllOpenOrdersPopup(orders: Orders): Observable<any> {
    this.logger.debug(`MessageService: Show notification with all open orders: ${JSON.stringify(orders._embedded.orderDtoList)}`);
    return this.dialog.open(MessageComponent, {
      data: {
        type: "all orders",
        orders: orders._embedded.orderDtoList,
      }
    }).afterClosed();
  }

  showErrorPopUp(error: string) {
    this.dialog.open(MessageComponent, {
      data: {
        type: "error",
        info: error
      }
    });
    this.logger.debug(`MessageService: Showing error popup: ${error}`);
  }

  showSummaryPopup(order: Order) {
    this.dialog.open(MessageComponent, {
      data: {
        type: "summary",
        order: order
      }
    });
    this.logger.debug(`MessageService: Showing notification with ride summary`);
  }

  showSummaryPopupWithRating(order: Order): Observable<any> {
    this.logger.debug(`MessageService: Showing summary with rating`);
    return this.dialog.open(MessageComponent, {
      data: {
        type: "summary-passenger",
        order: order
      }
    }).afterClosed();
  }

  showPopup(type: string, message: [string, string], order: Order): Observable<any> {
    this.logger.debug(`MessageService: Sent notification with title: ${message[0]} and body ${message[1]}`);
    return this.dialog.open(MessageComponent, {
      data: {
        type: type,
        title: message[0],
        info: message[1],
        order: order
      }
    }).afterClosed();
  }

  showInfoPopup(order: Order, action: string, type: string): Observable<any> {
    let message = this.selectPopupMessageFromStatus(action, order);
    if (message[0] && message[1]) {
      return this.showPopup(type, message, order);
    } else return null;
  }

  selectPopupMessageFromStatus(action: string, order: Order): [string, string] {
    let title: string;
    let message: string;
    switch (action) {
      case "accept":
        title = `Your ride has been accepted by ${order.driver.firstname}!`;
        message = `You can still cancel it but there will be a penalty fee of ${(order.cost / 2).toFixed(2)} EUR`;
        break;
      case "start":
        title = `Your ride has been started!`;
        message = ``;
        break;
      // case "summary":
      //   title = "Rating";
      //   message = `Passenger has rated the ride as ${this.rating[order.rating]}!`;
      //   break;
      case "complete":
        title = "You have arrived!";
        message = `Ride fee is: ${(order.cost).toFixed(2)} EUR`;
        break;
      case "cancel":
        title = "Passenger has cancelled the ride";
        message = `You will get a refund of ${(order.cost).toFixed(2)} EUR`;
        break;
    }
    return [title, message];
  }

  showConfirmationPopup(action: string, costEstimation: number, order: Order): Observable<any> {
    let message = this.selectPopupMessageFromAction(action, costEstimation);
    return this.showPopup("confirmation", message, order)
  }

  selectPopupMessageFromAction(action: string, costEstimation: number): [string, string] {
    let title: string;
    let message: string;
    switch (action) {
      case 'cancel':
        title = `Do you really want to cancel the order?`;
        message = `There will be a penalty fee of ${(costEstimation / 2).toFixed(2)} EUR`;
        break;
      case 'complete':
        title = 'Are you ready to complete the order?';
        message = 'Please confirm';
        break;
      case 'start':
        title = 'Are you ready to start?';
        message = 'Please confirm';
        break;
      case 'payment':
        title = `Are you ready to pay ${(costEstimation).toFixed(2)} EUR?`;
        message = `At the moment we accept only cash payments`;
        break;
      case 'accept':
        title = `Are you ready to accept the order?`;
        message = 'Please confirm';
        break;
      case 'terminate':
        title = `Do you really want to terminate the order?`;
        message = 'Please confirm';
        break;
      case 'create':
        title = 'Are you ready to order a taxi?';
        message = `Estimated price: ${(costEstimation).toFixed(2)} EUR`;
        break;
    }
    this.logger.debug(`MessageService: Selected popup message ${title}, ${message} from action ${action}`);
    return [title, message];
  }
}