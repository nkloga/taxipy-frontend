import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from "../models/user";
import { map } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { MenuService } from './menu.service';
import { WebSocketService } from './web-socket.service';
import { NGXLogger } from 'ngx-logger';
import { ImageFile } from '../models/file';
import { LostItem } from '../models/lostItem';
import { environment } from '../../environments/environment';

const WEBSOCKET_URL = `${environment.production ? 'ws' : 'wss'}://${environment.url.split('//')[1]}/socket`;
const EXAMPLE_URL = "/topic/server-broadcaster";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public baseUrl = environment.url;
  public currentUser: User = new User();
  public authorized: boolean = false;



  private authSource = new BehaviorSubject(this.authorized);
  authStatus = this.authSource.asObservable();

  private userSource = new BehaviorSubject(this.currentUser);
  userStatus = this.userSource.asObservable();

  constructor(private http: HttpClient,
    private webSocketService: WebSocketService,
    private menuService: MenuService,
    private logger: NGXLogger) { }

  getUser(id: number): Observable<Object> {
    this.logger.debug(`UserService: Requesting user by id: ${id}`);
    return this.http.get(`${this.baseUrl}/user/${id}`);
  }

  getProfilePicture(): Observable<any> {
    return this.http.get(`${this.baseUrl}/user/downloadDriverPhoto`)
      .pipe(map((data => <ImageFile>data)));
  }

  reportLostItem(item: LostItem): Observable<any> {
    let userType: string = null;
    if (this.currentUser.driver === null) {
      userType = 'lost';
    } else if (this.currentUser.passenger === null) {
      userType = 'found';
    }
    if (userType !== null) {
      return this.http.post(`${this.baseUrl}/${userType}/submit`, item).pipe(map((data => <LostItem>data)));
    }
    else {
      this.logger.error(`UserService: userType is not found`);
    }
  }

  updateUserData(user: User) {
    this.authSource.next(true);
    this.userSource.next(user);
    this.currentUser = user;
    this.menuService.updateMenu();
    this.menuService.setCurrentUser(user);
    this.initiateBroadcasting();
    this.logger.debug(`UserService: Authorizing user ${JSON.stringify(user)}`);
    this.logger.debug(`UserService: Updated menu, set current user`);
  }

  registerUser(user: User): Observable<User> {
    this.logger.debug(`UserService: Registering user ${JSON.stringify(user)}`);
    return this.http.post(`${this.baseUrl}/user/register`, user).pipe(map((data => <User>data)));
  }

  updateUser(user: User): Observable<Object> {
    this.logger.debug(`UserService: Updating the user ${JSON.stringify(user)}`);
    return this.http.post(`${this.baseUrl}/user/update`, user);
  }

  getCurrentUser(): Observable<User> {
    this.logger.debug(`UserService: Getting user`);
    return this.http.get(`${this.baseUrl}/user/profile`).pipe(map((data => <User>data)));
  }

  login(user: User): Observable<any> {
    this.logger.debug(`UserService: Log in user ${JSON.stringify(user)}`);
    const body = JSON.stringify({ username: user.username, password: user.password });
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post(`${this.baseUrl}/auth`, body, { headers: headers }
    );
  }

  authorizeUser(): Observable<User> {
    this.logger.debug(`UserService: Authorizing user`);
    let token = localStorage.getItem('token');

    let headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    return this.http.get(`${this.baseUrl}/user`, { headers: headers }).pipe(map((data => <User>data)));
  }

  initiateBroadcasting() {
    this.logger.debug(`UserService: Initiating broadcasting via web socket`);
    this.webSocketService.init(WEBSOCKET_URL, EXAMPLE_URL);
    this.menuService.monitorBroadcastMsg();
  }

  getUsersList(): Observable<any> {
    this.logger.debug(`UserService: Requesting user list`);
    return this.http.get(`${this.baseUrl}/user/list`);
  }
}