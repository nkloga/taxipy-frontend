import { Component } from '@angular/core';
import { Observable } from "rxjs";
import { UserService } from "../services/user.service";
import { User } from "../models/user";
import { Passenger } from "./../models/passenger";
import { Router } from '@angular/router';
import { NGXLogger } from 'ngx-logger';
import { Driver } from '../models/driver';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.css']
})
export class RegisterUserComponent {
  users: Observable<User[]>;
  submitted = false;
  model: any = {};
  user: User;
  error: string;
  minExpirationDate = new Date();
  maxExpirationDate = new Date(this.minExpirationDate.getFullYear() + 15, 0, 1);
  minBirthdayDate = new Date(this.minExpirationDate.getFullYear() - 100, 0, 1);
  maxBirthdayDate = new Date(this.minExpirationDate.getFullYear() - 18, this.minExpirationDate.getMonth(), this.minBirthdayDate.getDay());
  registrationConfirmation: boolean = false;
  typePassenger: boolean;

  constructor(private userService: UserService, private router: Router, private logger: NGXLogger) {
    this.typePassenger = true;
  }

  changeToPassengerForm() {
    this.typePassenger = true;
  }

  changeToDriverForm() {
    this.typePassenger = false;

  }

  onSubmitRider() {
    let role: string;
    let driver: Driver;
    let passenger: Passenger;
    if (this.typePassenger) {
      role = "ROLE_PASSENGER";
      driver = null;
      passenger = {
        birthdate: this.model.birthDate,
        location: this.model.location,
        country: this.model.country,
        creditCardNumber: this.model.creditCardNumber,
        expirationDate: this.model.expirationDate,
        active: true,
        ccv: this.model.ccv
      };
    } else {
      role = "ROLE_DRIVER";
      passenger = null;
      driver = {
        licenseNumber: this.model.licenseNumber,
        driversPhoto: null,
        carPlateNumber: this.model.carPlateNumber,
        carManufacturingYear: this.model.carManufacturingYear,
        carModel: this.model.carModel,
        amountOfSeats: this.model.amountOfSeats,
        carColor: this.model.carColor,
        active: true
      }
    }

    this.typePassenger ? role = "ROLE_PASSENGER" : role = "ROLE_DRIVER";
    this.user = {
      firstname: this.model.firstname,
      lastname: this.model.lastname,
      username: this.model.username,
      password: this.model.password,
      email: this.model.email,
      phoneNumber: this.model.phoneNumber,
      enabled: true,
      driver: driver,
      passenger: passenger,
      authorities: [{ authority: role }]
    }
    this.save();
    this.logger.debug(`RegisterUserComponent: Saving the user: ${JSON.stringify(this.user)}`)
  }

  save() {
    this.userService.registerUser(this.user)
      .subscribe(data => {
        this.submitted = true;
        this.logger.debug(`RegisterUserComponent: Registered user: ${JSON.stringify(data)}`)
        setTimeout(() => {
          this.router.navigate(['']);
        }, 3000);  //5s
      }, error => {
        this.submitted = false;
        console.log(error);
        this.error = error.error;
        this.logger.error(`RegisterUserComponent: ${error.error}`)
        // this.router.navigate(['']);
      });
  }
}