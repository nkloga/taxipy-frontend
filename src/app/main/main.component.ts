import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { LocationService } from '../services/location.service';
import { MenuService } from '../services/menu.service';
import { Coordinates } from '../models/coordinates';
import { NGXLogger } from 'ngx-logger';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
  encapsulation: ViewEncapsulation.None,

})
export class MainComponent implements OnInit {
  public origin: {
    lat: number,
    lng: number
  }
  public destination: any;
   lat: number;
   lng: number;
   zoom: number = 15;
   navigationLinks: Coordinates;

  constructor(private locationService: LocationService, private menuService: MenuService, private logger: NGXLogger, private router: Router) { }

  ngOnInit() {
    this.locationService.navigationStatus.subscribe(links => {
      this.logger.debug(`MainComponent: Received navigation coordinates from locationService.navigationStatus: ${JSON.stringify(links)}`);
      this.navigationLinks = links;
      if (links) {
        this.origin = { lat: Number(links.fromLat), lng: Number(links.fromLong) };
        this.destination = { lat: Number(links.whereLat), lng: Number(links.whereLong) };
        this.logger.debug(`MainComponent: Set navigation links to ${JSON.stringify(this.origin)} and ${JSON.stringify(this.destination)}`);
      }
      // this.locationService.setCurrentPosition();
    }, error => {
      this.router.navigate(['']);
      this.logger.error(`MainComponent: Error receiving navigation coordinates`);
      console.log(error)

    });

    this.locationService.setCurrentPosition();
    this.locationService.currentCoords.subscribe(coords => {
      this.logger.debug(`MainComponent: Received current coordinates from locationService.currentCoords: ${JSON.stringify(coords)}`);
      if (coords.whereLat && coords.whereLong) {
        this.lat = coords.whereLat;
        this.lng = coords.whereLong;
      } else if (coords.fromLat, coords.fromLong) {
        this.lat = coords.fromLat;
        this.lng = coords.fromLong;
      }
    }, error => {
      this.router.navigate(['']);
      this.logger.error(`MainComponent: Error receiving current coordinates`);
      console.log(error)
    });
  }

  icon = {
    url: './assets/images/pointer.png',
    scaledSize: {
      width: 40,
      height: 40
    }
  };

  styles = [{
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [{
      "color": "#e9e9e9"
    },
    {
      "lightness": 17
    }
    ]
  },
  {
    "featureType": "landscape",
    "elementType": "geometry",
    "stylers": [{
      "color": "#f5f5f5"
    },
    {
      "lightness": 20
    }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry.fill",
    "stylers": [{
      "color": "#ffffff"
    },
    {
      "lightness": 17
    }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry.stroke",
    "stylers": [{
      "color": "#ffffff"
    },
    {
      "lightness": 29
    },
    {
      "weight": 0.2
    }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "geometry",
    "stylers": [{
      "color": "#ffffff"
    },
    {
      "lightness": 18
    }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "geometry",
    "stylers": [{
      "color": "#ffffff"
    },
    {
      "lightness": 16
    }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [{
      "color": "#f5f5f5"
    },
    {
      "lightness": 21
    }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [{
      "color": "#dedede"
    },
    {
      "lightness": 21
    }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [{
      "visibility": "on"
    },
    {
      "color": "#ffffff"
    },
    {
      "lightness": 16
    }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [{
      "saturation": 36
    },
    {
      "color": "#333333"
    },
    {
      "lightness": 40
    }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [{
      "visibility": "off"
    }]
  },
  {
    "featureType": "transit",
    "elementType": "geometry",
    "stylers": [{
      "color": "#f2f2f2"
    },
    {
      "lightness": 19
    }
    ]
  },
  {
    "featureType": "administrative",
    "elementType": "geometry.fill",
    "stylers": [{
      "color": "#fefefe"
    },
    {
      "lightness": 20
    }
    ]
  },
  {
    "featureType": "administrative",
    "elementType": "geometry.stroke",
    "stylers": [{
      "color": "#fefefe"
    },
    {
      "lightness": 17
    },
    {
      "weight": 1.2
    }
    ]
  }
  ]
}

