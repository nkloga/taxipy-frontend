import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { User } from '../models/user';
import { LostItem } from '../models/lostItem';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lostitem',
  templateUrl: './lostitem.component.html',
  styleUrls: ['./lostitem.component.css']
})
export class LostitemComponent implements OnInit {

  constructor(private userService: UserService, private router: Router) {
    this.model = this.userService.currentUser;
  }

  ngOnInit() {
  }
  submittedLostItem = false;
  model: User = new User();
  lostItem: LostItem = new LostItem();
  minManDate = new Date(new Date().getFullYear()-1, 0, 1);
  maxManDate = new Date();

  onSubmitLostItem() {
    this.userService.reportLostItem(this.lostItem).subscribe(result => {
      this.submittedLostItem = true;
      setTimeout(() => {
        this.submittedLostItem = false;
      }, 3000);  //5s
    }, error => {
      this.submittedLostItem = false;
      this.router.navigate(['']);
    });
  }
}