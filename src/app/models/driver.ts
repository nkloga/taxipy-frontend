
export class Driver {
    licenseNumber: string;
    driversPhoto: string;
    carPlateNumber: string;
    carManufacturingYear: string;
    carModel: string;
    amountOfSeats: string;
    carColor: string;
    active: boolean;
}