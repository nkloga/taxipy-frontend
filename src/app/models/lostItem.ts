import { Passenger } from './passenger';

export class LostItem {
    description: string;
    date: string;
    passenger: Passenger;
    status: string;
}