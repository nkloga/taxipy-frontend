
export class ImageFile {
    fileDownloadUri: string;
    fileName: string;
    fileType: string;
    size: number;
}