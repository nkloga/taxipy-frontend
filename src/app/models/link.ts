
export class Link {
    complete: { href: string }
    accept: { href: string }
    orders: { href: string }
    self: { href: string }
    terminate: { href: string }
    cancel: { href: string }
    start: { href: string }
    payment: { href: string }
    create: { href: string }
    getOpen: { href: string }
}