import { Link } from './link';;
import { User } from './user';

export class Order {
    orderId: number;
    cost: number;
    distance: number;
    driver: User;
    rating: number;
    fromLat: number;
    fromLong: number;
    passenger: User;
    date: Date;
    status: string;
    time: number;
    whereLat: number;
    whereLong: number;
    _links: Link;
    fromName: string;
    whereName: string;
}

