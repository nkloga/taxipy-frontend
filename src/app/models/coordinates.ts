export class Coordinates {
  fromLat: number;
  fromLong: number;
  whereLat: number;
  whereLong: number;
  fromName: string;
  whereName: string;
}