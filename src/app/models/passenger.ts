
export class Passenger {
    birthdate: string;
    location: string;
    country: string;
    creditCardNumber: string;
    expirationDate: string;
    active: boolean;
    ccv: string;
}