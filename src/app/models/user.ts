import { Authority } from './authority';
import { Driver } from './driver';
import { Passenger } from './passenger';

export class User {
    firstname: string;
    lastname: string;
    username: string;
    password: string;
    email: string;
    phoneNumber: string;
    authorities: Authority[];
    enabled: boolean;
    driver: Driver;
    passenger: Passenger;
}