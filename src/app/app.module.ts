import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterUserComponent } from './register-user/register-user.component';
import { HttpClientModule } from '@angular/common/http';
import { OrderComponent } from './order/order.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { MaterialModule } from './material.module';
import { MainComponent } from './main/main.component';
import { AgmCoreModule } from '@agm/core';
import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';
import { MenuComponent } from './menu/menu.component';
import { OrderService } from './services/order.service';
import { UserService } from './services/user.service';
import { MenuService } from './services/menu.service';
import { AgmDirectionModule } from 'agm-direction';   // agm-direction
import { MessageComponent } from './message/message.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from '../app/auth/token.interceptor'
import { AuthService } from './auth/auth.service';
import { WebSocketService } from './services/web-socket.service';
import { LoggerModule, NgxLoggerLevel } from 'ngx-logger';
import { ProfileComponent } from './profile/profile.component';
import { RatingModule } from 'ng-starrating';
import { LostitemComponent } from './lostitem/lostitem.component';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    RegisterUserComponent,
    OrderComponent,
    LandingPageComponent,
    MainComponent,
    MenuComponent,
    MessageComponent,
    ProfileComponent,
    LostitemComponent
  ],
  imports: [
    RatingModule,
    FormsModule,
    AgmDirectionModule,
    BrowserModule,
    MaterialModule,
    HttpClientModule,
    AppRoutingModule,
    LoggerModule.forRoot({
      // serverLoggingUrl: '/api/logs',
      // level: NgxLoggerLevel.TRACE,
      level: environment.production ? NgxLoggerLevel.TRACE : NgxLoggerLevel.OFF,
      serverLogLevel: NgxLoggerLevel.OFF,
      disableConsoleLogging: false
    }),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAuhHkTa3ypLDQcSg6JQgFKOwgGeF0ngDw',
      libraries: ['places']
    }),
    MatGoogleMapsAutocompleteModule,
  ],
  providers: [UserService, OrderService, MenuService, AuthService, WebSocketService, {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent],
  entryComponents: [
    MessageComponent
  ]
})
export class AppModule { }
