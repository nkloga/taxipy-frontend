import { Component, OnInit } from '@angular/core';
import { UserService } from "../services/user.service";
import { User } from "../models/user";
import { Router } from '@angular/router';
import { NGXLogger } from 'ngx-logger';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit {

   user: User = new User();
   submitted = false;
 error: string;

  constructor(private userService: UserService, private router: Router, private logger: NGXLogger) { }
  ngOnInit() {
  }

  onSubmit() {
    this.userService.login(this.user).subscribe(
      token => {
        localStorage.setItem('token', token.token);
        this.logger.debug("LandingPageComponent: Setting token to the localStorage: " + localStorage.getItem('token'));
        this.userService.authorizeUser().subscribe(
          user => {
            this.logger.debug("LandingPageComponent: Authorized user: " + JSON.stringify(user));
            if (user.enabled) {
              this.userService.updateUserData(user);
              setTimeout(() => {
                this.router.navigate(['/main']);
              }, 1000);
              this.submitted = true;
            } else {
              this.logger.error('LandingPageComponent: User is not authorized');
            }
          }, error => {
            this.error = error.error;
            this.router.navigate(['']);
          });
        ;
      }, error => {
        this.error = "User name or password is incorrect!";
        this.logger.error(`LandingPageComponent: Auth error: ${JSON.stringify(error)}`);
        this.router.navigate(['']);
      })
  }
}