import { Component, OnInit } from '@angular/core';
import { UserService } from "../services/user.service";
import { User } from "./../models/user";
import { ImageFile } from "./../models/file";
import { Router } from '@angular/router';
import { NGXLogger } from 'ngx-logger';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { OrderService } from '../services/order.service';
import { Order } from '../models/order';
import { MessageService } from '../services/message.service';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  ngOnInit() { }

  public baseUrl = environment.url;
  activeUser: User;
  user: User;
  submitted = false;
  model: User = new User();
  error: string;
  registrationConfirmation: boolean;
  selectedFile: File;
  imageUrl: string;
  orders: Order[];


  constructor(private userService: UserService, private messageService: MessageService, private orderService: OrderService, private logger: NGXLogger, private http: HttpClient, private router: Router) {
    this.model = this.userService.currentUser;
    userService.getProfilePicture().subscribe(result => {
      if (result.fileDownloadUri !== null) {
        this.imageUrl = this.baseUrl + result.fileDownloadUri;
      }
    }, error => {
      this.logger.error(`ProfileComponent: Couldnt get user picture: ${JSON.stringify(error)}`)
      this.router.navigate(['']);
    });
    orderService.getOrdersHistory().subscribe(result => {
      if (result._embedded) {
        this.orders = result._embedded.orderDtoList;
      }
    }, error => {
      this.logger.error(`ProfileComponent: Couldnt get orders history: ${JSON.stringify(error)}`)
      this.router.navigate(['']);
    });
  }

  onSubmit() {
    this.save();
    this.logger.debug(`ProfileComponent: Updated the user: ${JSON.stringify(this.model)}`)
  }

  openOrderPopup(order: Order) {
    this.messageService.showSummaryPopup(order);
  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0]
  }

  onUpload() {
    const uploadData = new FormData();
    uploadData.append('file', this.selectedFile, this.selectedFile.name);
    this.http.post(`${this.baseUrl}/user/uploadDriverPhoto`, uploadData).pipe(map((data => <ImageFile>data)))
      .subscribe(result => {
        this.imageUrl = this.baseUrl + result.fileDownloadUri;
        this.logger.error(`ProfileComponent: Uploaded user picture: ${JSON.stringify(this.imageUrl)}`);
      }, error => {
        this.logger.error(`ProfileComponent: Couldnt upload user picture: ${JSON.stringify(error)}`);
      });
  }

  getProfilePicture(id: string): Observable<any> {
    return this.http.get(id);
  }

  save() {
    this.userService.updateUser(this.model)
      .subscribe(data => {
        this.logger.debug(`ProfileComponent: updated the user response from backend: ${JSON.stringify(data)}`)
        this.submitted = true;
        setTimeout(() => {
          this.submitted = false;
        }, 3000);  //5s
      }, error => {
        this.submitted = false;
        this.error = error.error.message;
        this.logger.error(`ProfileComponent: ${this.error}`);
        this.router.navigate(['']);
      });
  }
}