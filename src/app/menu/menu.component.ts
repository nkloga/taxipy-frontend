import { Component, Input, OnInit } from '@angular/core';
import { OrderService } from '../services/order.service';
import { LocationService } from '../services/location.service';
import { Link } from "../models/link";
import { User } from "../models/user";
import { Location } from '@angular-material-extensions/google-maps-autocomplete';
import { Coordinates } from '../models/coordinates';
import { UserService } from '../services/user.service';
import { MenuService } from '../services/menu.service';
import { MatDialog, MatDialogRef } from '@angular/material';
import { MessageComponent } from '../message/message.component';
import { NGXLogger } from 'ngx-logger';
import { MessageService } from '../services/message.service';
import { Order } from '../models/order';
import { Router } from '@angular/router';
const MAX_DISTANCE = 30;

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  @Input() action: string;
  fileNameDialogRef: MatDialogRef<MessageComponent>;
  private links: Link = new Link();
  loggedIn: boolean = false;
  private currentUser: User;
  private validDestination: boolean = true;
  public inputMessage: String;
  private menu: String;
  showSearchBar = false;

  coordinates: Coordinates = new Coordinates();

  constructor(private orderService: OrderService,
    private locationService: LocationService,
    private userService: UserService,
    private menuService: MenuService,
    private messageService: MessageService,
    private logger: NGXLogger,
    private router: Router,
    private dialog: MatDialog) { }

  ngOnInit() {
    this.userService.authStatus.subscribe(status => {
      this.logger.debug(`MenuComponent: Received update from userService.authStatus: ${JSON.stringify(status)}`);
      this.loggedIn = status;
    });
    this.userService.userStatus.subscribe(user => {
      this.logger.debug(`MenuComponent: Received update from userService.userStatus: ${JSON.stringify(user)}`);
      this.currentUser = user;
    });
    this.menuService.linksStatus.subscribe(links => {
      this.logger.debug(`MenuComponent: Received update from menuService.linksStatus: ${JSON.stringify(links)}`);
      this.links = links;
    });
    this.locationService.currentCoords.subscribe(coords => {
      this.logger.debug(`MenuComponent: Received update from locationService.currentCoords: ${JSON.stringify(coords)}`);
      this.coordinates = coords;
    });
    this.orderService.orderStatus.subscribe(order => {
      this.logger.debug(`MenuComponent: Received update from orderService.orderStatus: ${JSON.stringify(order)}`);
      this.menuService.lastOrder = order;
      this.updateSearchBar();
    });
    this.menu = "main";
  }

  updateSearchBar() {
    if (this.loggedIn && this.menuService.lastOrder === null && this.currentUser.authorities[0].authority === "ROLE_PASSENGER") {
      this.showSearchBar = true;
    } else {
      this.showSearchBar = false;
    }
  }

  logout() {
    // this.webSocketService.disconnect();
  }

  updateCoords() {
    this.logger.debug(`MenuComponent: Updating coordinates in locationService with: ${this.coordinates}`);
    this.locationService.changeCoordinates(this.coordinates);
  }

  executeAction(action: string) {
    this.logger.debug(`MenuComponent: Executing action: ${action}`);
    if (action === 'create' && !this.checkDestinationCoordinates()) {
      this.logger.debug(`MenuComponent: Selected destination is not valid`);
      this.locationService.resetDestinationCoordinates();
    } else if (action === 'getOpen') {
      this.generateAllOpenOrdersPopup()
    } else {
      let costEstimation = this.orderService.calculateCost(this.locationService.calculateDistance(this.coordinates));
      if (this.currentUser.driver === null && action === "cancel") {
        costEstimation = 0;
      }
      this.messageService.showConfirmationPopup(action, costEstimation, this.menuService.lastOrder).subscribe(confirmation => {
        this.logger.debug(`MenuComponent: Popup has been closed, received object: ${JSON.stringify(confirmation)}`);
        if (confirmation) {
          this.menuService.executeAction(action);
        }

      }, error => {
        this.logger.debug(`MenuComponent: Error getting message result: ${JSON.stringify(error)}`);
        this.router.navigate(['']);
      });
    }
    this.cleanUpInputField();
  }

  cleanUpInputField() {
    this.inputMessage = "";
  }

  openProfilePage() {
    this.menu = 'profile';
    this.router.navigate(['/profile']);
  }

  returnToMain() {
    this.menu = 'main';
    this.router.navigate(['/main']);
  }

  destinationIsTooFar(): boolean {
    let valid: boolean;
    let distance = this.locationService.calculateDistance(this.coordinates);
    if (distance > MAX_DISTANCE) {
      valid = true;
    } else {
      this.updateCoords();
      valid = false;
    }
    this.logger.debug(`MenuComponent: Selected destination is valid: ${this.validDestination}`);
    return valid;
  }

  destinationCoordsNotExist(): boolean {
    if (!this.coordinates.whereLat || !this.coordinates.whereLong) {
      return true;
    } else return false;
  }

  generateAllOpenOrdersPopup() {
    this.menuService.getListOfOpenOrders().subscribe(
      orders => {
        this.logger.debug(`MenuComponent: Received list of open orders ${JSON.stringify(orders)}`);
        if (orders._embedded) {
          this.logger.debug(`MenuComponent: Orders._embedded equals to ${JSON.stringify(orders._embedded)}`);
          this.messageService.showAllOpenOrdersPopup(orders)
            .subscribe(selectedOrder => {
              if (selectedOrder) {
                this.acceptOrder(selectedOrder);
                this.logger.debug(`MenuComponent: Popup has been closed, accepted order: ${JSON.stringify(selectedOrder)}`);
              }
            }, error => {
              this.logger.error(`MenuComponent: Error getting message result: ${JSON.stringify(error)}`);
              this.router.navigate(['']);
            });
        }
      }, error => {
        this.logger.error(`MenuComponent: Error getting list of orders: ${JSON.stringify(error)}`)
        this.router.navigate(['']);
      });
  }

  acceptOrder(selectedOrder: Order) {
    this.orderService.getOrderById(selectedOrder.orderId).subscribe(order => {
      this.logger.debug(`MenuComponent: requested order by id, received: ${JSON.stringify(order)}`);
      this.menuService.updateLinks(order);
      this.menuService.executeAction('accept');
    }, error => {
      this.logger.error(`MenuComponent: Error getting order by id: ${JSON.stringify(error)}`)
      this.router.navigate(['']);
    });
  }

  setDestinationCoordinates(location: Location) {
    this.coordinates.whereLat = location.latitude;
    this.coordinates.whereLong = location.longitude;
  }

  checkDestinationCoordinates(): boolean {
    let valid: boolean = false;
    if (this.destinationCoordsNotExist()) {
      this.messageService.showErrorPopUp("Please choose your destination address");
    } else if (this.destinationIsTooFar()) {
      this.messageService.showErrorPopUp("Sorry, but the destination is too far from your current location");
    } else {
      this.logger.debug(`MenuComponent: Set destination coordinates to: ${JSON.stringify(location)}`);
      valid = true;
    }
    return valid;
  }
}