import { Order } from '../models/order';

export interface DialogData {
  type: string;
  action: string;
  info: string;
  title: string;
  orders: Order[];
  order: Order;
  cost: number;
}