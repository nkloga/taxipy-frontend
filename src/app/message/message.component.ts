import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogData } from './DialogData';
import { Order } from "../models/order";
import { NGXLogger } from 'ngx-logger';
import { environment } from '../../environments/environment';


@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})

export class MessageComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<MessageComponent>, @Inject(MAT_DIALOG_DATA) public data: DialogData, private logger: NGXLogger) { }

  messageTitle: string;
  messageText: string;
  type: string;
  orders: Order[];
  order: Order;
  imageUrl: string;
  public rating = ["TERRIBLE", "DISAPPOINTMENT", "OK", "GOOD", "PERFECT"];
  public feedback: string;

  ngOnInit() {
    this.type = this.data.type;
    switch (this.data.type) {
      case "all orders":
        this.orders = this.data.orders;
        break;
      case "error":
        this.messageTitle = this.data.info;
        break;
      case "accept":
        this.messageTitle = this.data.title;
        this.messageText = this.data.info;
        this.order = this.data.order;
        if (this.data.order.driver.driver.driversPhoto) {
          this.imageUrl = `${environment.url}/user/downloadDriverPhoto/${this.data.order.driver.driver.driversPhoto}`;
        }
        break;
      default:
        this.messageTitle = this.data.title;
        this.messageText = this.data.info;
        this.order = this.data.order;
        break;
    }
  }

  // window.addEventListener("beforeunload", function() { debugger; }, false)

  acceptOrder(order: Order) {
    this.dialogRef.close(order);
    this.logger.debug(`MessageComponent: accepted order: ${JSON.stringify(order)}`);
  }

  onClickClose(): void {
    this.dialogRef.close();
    this.logger.debug(`MessageComponent: closed popup`);
  }

  onClickConfirm(): void {
    this.dialogRef.close(true);
    this.logger.debug(`MessageComponent: popup confirmation received`);
  }

  // onRate($event: { oldValue: number, newValue: number, starRating: StarRatingComponent }) {
  //   this.dialogRef.close($event.newValue - 1);
  // }

  onSetRating(rating: number): void {
    this.dialogRef.close(rating - 1);
    this.logger.debug(`MessageComponent: closed popup, set rating ${rating - 1}`);
  }
}