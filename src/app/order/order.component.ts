import { Component } from '@angular/core';
import { OrderService } from "../services/order.service";
import { Order } from "../models/order";
import { NGXLogger } from 'ngx-logger';
import { Router } from '@angular/router';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})

export class OrderComponent {
  private order: Order = new Order();
  private orders: Order[];

  constructor(private orderService: OrderService, private logger: NGXLogger, private router: Router) { }

  getActiveOrder() {
    this.orderService.getActiveOrder().subscribe(order => {
      this.order = order;
      this.logger.debug(`OrderComponent: received active order: ${JSON.stringify(order)}`);
    }, error => {
      this.logger.error(`OrderComponent: error receiving an active order: ${JSON.stringify(error)}`);
      this.router.navigate(['']);
    });
  }

  getOpenOrders() {
    this.orderService.getOpenOrders().subscribe(orders => {
      this.orders = orders._embedded.orderDtoList;
      this.logger.debug(`OrderComponent: received open orders: ${JSON.stringify(orders)}`);
    }, error => {
      this.logger.error(`OrderComponent: error receiving open orders: ${JSON.stringify(error)}`);
      this.router.navigate(['']);
    });
  }
}