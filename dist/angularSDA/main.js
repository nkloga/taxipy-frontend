(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _register_user_register_user_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./register-user/register-user.component */ "./src/app/register-user/register-user.component.ts");
/* harmony import */ var _main_main_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./main/main.component */ "./src/app/main/main.component.ts");
/* harmony import */ var _profile_profile_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./profile/profile.component */ "./src/app/profile/profile.component.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _order_order_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./order/order.component */ "./src/app/order/order.component.ts");
/* harmony import */ var _landing_page_landing_page_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./landing-page/landing-page.component */ "./src/app/landing-page/landing-page.component.ts");








var routes = [
    { path: '', component: _landing_page_landing_page_component__WEBPACK_IMPORTED_MODULE_7__["LandingPageComponent"] },
    { path: 'register', component: _register_user_register_user_component__WEBPACK_IMPORTED_MODULE_1__["RegisterUserComponent"] },
    { path: 'main', component: _main_main_component__WEBPACK_IMPORTED_MODULE_2__["MainComponent"] },
    { path: 'profile', component: _profile_profile_component__WEBPACK_IMPORTED_MODULE_3__["ProfileComponent"] },
    { path: 'order', component: _order_order_component__WEBPACK_IMPORTED_MODULE_6__["OrderComponent"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-menu></app-menu>\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _register_user_register_user_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./register-user/register-user.component */ "./src/app/register-user/register-user.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _order_order_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./order/order.component */ "./src/app/order/order.component.ts");
/* harmony import */ var _landing_page_landing_page_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./landing-page/landing-page.component */ "./src/app/landing-page/landing-page.component.ts");
/* harmony import */ var _material_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./material.module */ "./src/app/material.module.ts");
/* harmony import */ var _main_main_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./main/main.component */ "./src/app/main/main.component.ts");
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @agm/core */ "./node_modules/@agm/core/index.js");
/* harmony import */ var _angular_material_extensions_google_maps_autocomplete__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular-material-extensions/google-maps-autocomplete */ "./node_modules/@angular-material-extensions/google-maps-autocomplete/esm5/google-maps-autocomplete.es5.js");
/* harmony import */ var _menu_menu_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./menu/menu.component */ "./src/app/menu/menu.component.ts");
/* harmony import */ var _services_order_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./services/order.service */ "./src/app/services/order.service.ts");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _services_menu_service__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./services/menu.service */ "./src/app/services/menu.service.ts");
/* harmony import */ var agm_direction__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! agm-direction */ "./node_modules/agm-direction/fesm5/agm-direction.js");
/* harmony import */ var _message_message_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./message/message.component */ "./src/app/message/message.component.ts");
/* harmony import */ var _app_auth_token_interceptor__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ../app/auth/token.interceptor */ "./src/app/auth/token.interceptor.ts");
/* harmony import */ var _auth_auth_service__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./auth/auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var _services_web_socket_service__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./services/web-socket.service */ "./src/app/services/web-socket.service.ts");
/* harmony import */ var ngx_logger__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ngx-logger */ "./node_modules/ngx-logger/esm5/ngx-logger.js");
/* harmony import */ var _profile_profile_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./profile/profile.component */ "./src/app/profile/profile.component.ts");
/* harmony import */ var ng_starrating__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ng-starrating */ "./node_modules/ng-starrating/fesm5/ng-starrating.js");
/* harmony import */ var _lostitem_lostitem_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./lostitem/lostitem.component */ "./src/app/lostitem/lostitem.component.ts");


















 // agm-direction









var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
                _register_user_register_user_component__WEBPACK_IMPORTED_MODULE_6__["RegisterUserComponent"],
                _order_order_component__WEBPACK_IMPORTED_MODULE_8__["OrderComponent"],
                _landing_page_landing_page_component__WEBPACK_IMPORTED_MODULE_9__["LandingPageComponent"],
                _main_main_component__WEBPACK_IMPORTED_MODULE_11__["MainComponent"],
                _menu_menu_component__WEBPACK_IMPORTED_MODULE_14__["MenuComponent"],
                _message_message_component__WEBPACK_IMPORTED_MODULE_19__["MessageComponent"],
                _profile_profile_component__WEBPACK_IMPORTED_MODULE_24__["ProfileComponent"],
                _lostitem_lostitem_component__WEBPACK_IMPORTED_MODULE_26__["LostitemComponent"]
            ],
            imports: [
                ng_starrating__WEBPACK_IMPORTED_MODULE_25__["RatingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                agm_direction__WEBPACK_IMPORTED_MODULE_18__["AgmDirectionModule"],
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _material_module__WEBPACK_IMPORTED_MODULE_10__["MaterialModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClientModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
                ngx_logger__WEBPACK_IMPORTED_MODULE_23__["LoggerModule"].forRoot({
                    serverLoggingUrl: '/api/logs',
                    level: ngx_logger__WEBPACK_IMPORTED_MODULE_23__["NgxLoggerLevel"].TRACE,
                    serverLogLevel: ngx_logger__WEBPACK_IMPORTED_MODULE_23__["NgxLoggerLevel"].ERROR,
                    disableConsoleLogging: false
                }),
                _agm_core__WEBPACK_IMPORTED_MODULE_12__["AgmCoreModule"].forRoot({
                    apiKey: 'AIzaSyC09WQqDDbJDSjCWhPm8MelSytYzOjPtX0',
                    libraries: ['places']
                }),
                _angular_material_extensions_google_maps_autocomplete__WEBPACK_IMPORTED_MODULE_13__["MatGoogleMapsAutocompleteModule"],
            ],
            providers: [_services_user_service__WEBPACK_IMPORTED_MODULE_16__["UserService"], _services_order_service__WEBPACK_IMPORTED_MODULE_15__["OrderService"], _services_menu_service__WEBPACK_IMPORTED_MODULE_17__["MenuService"], _auth_auth_service__WEBPACK_IMPORTED_MODULE_21__["AuthService"], _services_web_socket_service__WEBPACK_IMPORTED_MODULE_22__["WebSocketService"], {
                    provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HTTP_INTERCEPTORS"],
                    useClass: _app_auth_token_interceptor__WEBPACK_IMPORTED_MODULE_20__["TokenInterceptor"],
                    multi: true
                }],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]],
            entryComponents: [
                _message_message_component__WEBPACK_IMPORTED_MODULE_19__["MessageComponent"]
            ]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/auth/auth.service.ts":
/*!**************************************!*\
  !*** ./src/app/auth/auth.service.ts ***!
  \**************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var jwt_decode__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jwt-decode */ "./node_modules/jwt-decode/lib/index.js");
/* harmony import */ var jwt_decode__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jwt_decode__WEBPACK_IMPORTED_MODULE_2__);



var AuthService = /** @class */ (function () {
    function AuthService() {
    }
    AuthService.prototype.getToken = function () {
        return localStorage.getItem('token');
    };
    AuthService.prototype.isAuthenticated = function () {
        // get the token
        var token = this.getToken();
        // return a boolean reflecting 
        // whether or not the token is expired
        return jwt_decode__WEBPACK_IMPORTED_MODULE_2___default.a.tokenNotExpired(null, token);
    };
    AuthService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/auth/token.interceptor.ts":
/*!*******************************************!*\
  !*** ./src/app/auth/token.interceptor.ts ***!
  \*******************************************/
/*! exports provided: TokenInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TokenInterceptor", function() { return TokenInterceptor; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./auth.service */ "./src/app/auth/auth.service.ts");



var TokenInterceptor = /** @class */ (function () {
    function TokenInterceptor(auth) {
        this.auth = auth;
    }
    TokenInterceptor.prototype.intercept = function (request, next) {
        if (this.auth.getToken()) {
            request = request.clone({
                setHeaders: {
                    Authorization: "Bearer " + this.auth.getToken()
                }
            });
        }
        return next.handle(request);
    };
    TokenInterceptor = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]])
    ], TokenInterceptor);
    return TokenInterceptor;
}());



/***/ }),

/***/ "./src/app/landing-page/landing-page.component.css":
/*!*********************************************************!*\
  !*** ./src/app/landing-page/landing-page.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xhbmRpbmctcGFnZS9sYW5kaW5nLXBhZ2UuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/landing-page/landing-page.component.html":
/*!**********************************************************!*\
  !*** ./src/app/landing-page/landing-page.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [hidden]=\"submitted\">\n  <section class=\"pb_xl_py_cover overflow-hidden bg-light pb_slant-white pb_gradient_v1 cover-bg-opacity-4 uber-bg\"\n    id=\"section-home\">\n    <div class=\"container\">\n      <div class=\"row align-items-center justify-content-center\">\n        <div class=\"col-md-6\">\n          <h2 class=\"heading mb-3\">Taxipy</h2>\n          <div class=\"sub-heading\">\n            <p class=\"mb-4\">The smartest way to move around in your city!</p>\n          </div>\n        </div>\n        <div class=\"col-md-1\">\n        </div>\n        <div class=\"col-md-5 relative align-self-center\">\n          <form class=\"bg-white rounded pb_form_v1\" (ngSubmit)=\"onSubmit()\">\n            <h2 class=\"mb-4 mt-0 text-center\">Login</h2>\n            <div class=\"form-group\">\n              <input type=\"text\" class=\"form-control pb_height-50 reverse\" placeholder=\"User name\" id=\"name\"\n                [(ngModel)]=\"user.username\" name=\"username\">\n            </div>\n            <div class=\"form-group\">\n              <input type=\"password\" class=\"form-control pb_height-50 reverse\" placeholder=\"Password\" id=\"password\"\n                [(ngModel)]=\"user.password\" name=\"password\">\n            </div>\n            <div class=\"form-group\">\n              <input type=\"submit\" class=\"btn btn-primary btn-lg btn-block pb_btn-pill  btn-shadow-blue\" value=\">\">\n            </div>\n            <div *ngIf=\"error\" class=\"server-error form-group\">{{error}}</div>\n          </form>\n        </div>\n      </div>\n    </div>\n  </section>\n\n  <section class=\"pb_section pb_slant-light\" id=\"section-features\">\n    <div class=\"container\">\n      <div class=\"row\">\n        <div class=\"col-lg-4 mb-5\">\n          <img src=\"assets/images/phone_5.png\" alt=\"Image placeholder\" class=\"img-fluid\">\n        </div>\n        <div class=\"col-lg-8 pl-md-5 pl-sm-0\">\n          <div class=\"row\">\n            <div class=\"col\">\n              <h2>Taxipy Features</h2>\n              <p class=\"pb_font-20\">For clients</p>\n            </div>\n          </div>\n          <div class=\"row\">\n            <div class=\"col-lg\">\n              <div class=\"media pb_feature-v2 text-left mb-1 mt-5\">\n                <div class=\"pb_icon d-flex mr-3 align-self-start pb_w-15\"><i\n                    class=\"ion-ios-bookmarks-outline pb_icon-gradient\"></i></div>\n                <div class=\"media-body\">\n                  <h3 class=\"mt-2 mb-2 heading\">Affordable and transparent rates</h3>\n                  <p class=\"text-sans-serif pb_font-16\">We guarantee the lowest price on the market</p>\n                </div>\n              </div>\n              <div class=\"media pb_feature-v2 text-left mb-1 mt-5\">\n                <div class=\"pb_icon d-flex mr-3 align-self-start pb_w-15\"><i\n                    class=\"ion-ios-infinite-outline pb_icon-gradient\"></i></div>\n                <div class=\"media-body\">\n                  <h3 class=\"mt-2 mb-2 heading\">Payment by cash or card</h3>\n                  <p class=\"text-sans-serif pb_font-16\">Pay be card, cash, bitcoins, write a check, or try to barter\n                    with the driver</p>\n                </div>\n              </div>\n            </div>\n            <div class=\"col-lg\">\n              <div class=\"media pb_feature-v2 text-left mb-1 mt-5\">\n                <div class=\"pb_icon d-flex mr-3 align-self-start pb_w-15\"><i\n                    class=\"ion-ios-speedometer-outline pb_icon-gradient\"></i></div>\n                <div class=\"media-body\">\n                  <h3 class=\"mt-2 mb-2 heading\">Smart app with hints</h3>\n                  <p class=\"text-sans-serif pb_font-16\">Our app is so smart, in fact it is much smarter that many people\n                    around</p>\n                </div>\n              </div>\n              <div class=\"media pb_feature-v2 text-left mb-1 mt-5\">\n                <div class=\"pb_icon d-flex mr-3 align-self-start pb_w-15\"><i\n                    class=\"ion-ios-color-filter-outline  pb_icon-gradient\"></i></div>\n                <div class=\"media-body\">\n                  <h3 class=\"mt-2 mb-2 heading\">Availability</h3>\n                  <p class=\"text-sans-serif pb_font-16\">We have thousands of drivers waiting for you</p>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </section>\n\n  <section class=\"pb_section bg-light pb_slant-white\" id=\"section-pricing\">\n    <div class=\"container\">\n      <div class=\"row justify-content-center mb-5\">\n        <div class=\"col-md-6 text-center mb-5\">\n          <h5 class=\"text-uppercase pb_font-15 mb-2 pb_color-dark-opacity-3 pb_letter-spacing-2\">\n            <strong>Pricing</strong></h5>\n          <h2>Choose Your Plans</h2>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-md\">\n          <div class=\"pb_pricing_v1 p-5 border text-center bg-white\">\n            <h3>Basic</h3>\n            <span class=\"price\"><sup>€</sup>0,19<span>km</span></span>\n            <p class=\"pb_font-15\">Economy - for everyday rides, affordable option for everyone</p>\n            <p class=\"mb-0\"><a href=\"#\" role=\"button\" class=\"btn btn-secondary\">Get started</a></p>\n          </div>\n        </div>\n        <div class=\"col-md\">\n          <div class=\"pb_pricing_v1 p-5 border border-primary text-center bg-white\">\n            <h3>Business</h3>\n            <span class=\"price\"><sup>€</sup>0,39<span>km</span></span>\n            <p class=\"pb_font-15\">Comfort - enjoy the ride and feel the comfort of a new car (cars from 2013 y.)</p>\n            <p class=\"mb-0\"><a href=\"#\" role=\"button\" class=\"btn btn-primary btn-shadow-blue\">Get started</a></p>\n          </div>\n        </div>\n        <div class=\"col-md\">\n          <div class=\"pb_pricing_v1 p-5 border text-center bg-white\">\n            <h3>Unlimited</h3>\n            <span class=\"price\"><sup>€</sup>0,99<span>km</span></span>\n            <p class=\"pb_font-15\">Premium - only luxury, premium cars, drinks and snacks, feel like a king, bro</p>\n            <p class=\"mb-0\"><a href=\"#\" role=\"button\" class=\"btn btn-secondary\">Get started</a></p>\n          </div>\n        </div>\n      </div>\n    </div>\n  </section>\n</div>\n\n<div class=\"container success-cont\" [hidden]=\"!submitted\">\n  <div class=\"row align-items-center justify-content-center\">\n    <div class=\"col-md-3\"></div>\n    <div class=\"col-md-6 relative align-self-center\">\n      <img src=\"assets/images/green.png\" class=\"success-img\" />\n      <h1 class=\"mb-4 mt-0 text-center\">You have successfully logged in!</h1>\n    </div>\n    <div class=\"col-md-3\"></div>\n  </div>\n</div>\n\n<footer class=\"pb_footer bg-white\" role=\"contentinfo\">\n  <div class=\"container\">\n    <div class=\"row text-center\">\n      <div class=\"col\">\n        <ul class=\"list-inline\">\n          <li class=\"list-inline-item\"><a href=\"#\" class=\"p-2\"><i class=\"fa fa-facebook\"></i></a></li>\n          <li class=\"list-inline-item\"><a href=\"#\" class=\"p-2\"><i class=\"fa fa-twitter\"></i></a></li>\n          <li class=\"list-inline-item\"><a href=\"#\" class=\"p-2\"><i class=\"fa fa-linkedin\"></i></a></li>\n        </ul>\n      </div>\n    </div>\n    <div class=\"row\">\n      <div class=\"col text-center\">\n        <p class=\"pb_font-14\"> 2019. All Rights Reserved. <br></p>\n      </div>\n    </div>\n  </div>\n</footer>\n\n<!-- loader -->\n<div id=\"pb_loader\" class=\"show fullscreen\"><svg class=\"circular\" width=\"48px\" height=\"48px\">\n    <circle class=\"path-bg\" cx=\"24\" cy=\"24\" r=\"22\" fill=\"none\" stroke-width=\"4\" stroke=\"#eeeeee\" />\n    <circle class=\"path\" cx=\"24\" cy=\"24\" r=\"22\" fill=\"none\" stroke-width=\"4\" stroke-miterlimit=\"10\" stroke=\"#1d82ff\" />\n  </svg></div>\n"

/***/ }),

/***/ "./src/app/landing-page/landing-page.component.ts":
/*!********************************************************!*\
  !*** ./src/app/landing-page/landing-page.component.ts ***!
  \********************************************************/
/*! exports provided: LandingPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LandingPageComponent", function() { return LandingPageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _models_user__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../models/user */ "./src/app/models/user.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ngx_logger__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-logger */ "./node_modules/ngx-logger/esm5/ngx-logger.js");






var LandingPageComponent = /** @class */ (function () {
    function LandingPageComponent(userService, router, logger) {
        this.userService = userService;
        this.router = router;
        this.logger = logger;
        this.user = new _models_user__WEBPACK_IMPORTED_MODULE_3__["User"]();
        this.submitted = false;
    }
    LandingPageComponent.prototype.ngOnInit = function () {
    };
    LandingPageComponent.prototype.onSubmit = function () {
        var _this = this;
        this.userService.login(this.user).subscribe(function (token) {
            localStorage.setItem('token', token.token);
            _this.logger.debug("LandingPageComponent: Setting token to the localStorage: " + localStorage.getItem('token'));
            _this.userService.authorizeUser().subscribe(function (user) {
                _this.logger.debug("LandingPageComponent: Authorized user: " + JSON.stringify(user));
                if (user.enabled) {
                    _this.userService.updateUserData(user);
                    setTimeout(function () {
                        _this.router.navigate(['/main']);
                    }, 1000);
                    _this.submitted = true;
                }
                else {
                    _this.logger.error('LandingPageComponent: User is not authorized');
                }
            }, function (error) {
                _this.error = error.error;
                _this.router.navigate(['']);
            });
            ;
        }, function (error) {
            _this.error = "User name or password is incorrect!";
            _this.logger.error("LandingPageComponent: Auth error: " + error);
            _this.router.navigate(['']);
        });
    };
    LandingPageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-landing-page',
            template: __webpack_require__(/*! ./landing-page.component.html */ "./src/app/landing-page/landing-page.component.html"),
            styles: [__webpack_require__(/*! ./landing-page.component.css */ "./src/app/landing-page/landing-page.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], ngx_logger__WEBPACK_IMPORTED_MODULE_5__["NGXLogger"]])
    ], LandingPageComponent);
    return LandingPageComponent;
}());



/***/ }),

/***/ "./src/app/lostitem/lostitem.component.css":
/*!*************************************************!*\
  !*** ./src/app/lostitem/lostitem.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xvc3RpdGVtL2xvc3RpdGVtLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/lostitem/lostitem.component.html":
/*!**************************************************!*\
  !*** ./src/app/lostitem/lostitem.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form name=\"form\" class=\"\" (ngSubmit)=\"onSubmitLostItem()\" [hidden]=\"submittedLostItem\" #f=\"ngForm\" novalidate>\n  <div class=\"form-group\">\n    <label for=\"lostItem\" *ngIf=\"model.passenger\">Describe the item you forgot in the taxi?</label>\n    <label for=\"lostItem\" *ngIf=\"model.driver\">Describe the item you found in the taxi?</label>\n    <input type=\"text\" class=\"form-control pb_height-50 reverse\" name=\"lostItem\" [(ngModel)]=\"lostItem.description\"\n      required />\n  </div>\n  <div class=\"form-group\">\n    <mat-form-field class=\"example-full-width\">\n      <input matInput [min]=\"minManDate\" [max]=\"maxManDate\" [matDatepicker]=\"manDate\" placeholder=\"Date of your ride\"\n        name=\"dateOfRide\" [(ngModel)]=\"lostItem.date\" #dateOfRide=\"ngModel\" required />\n      <mat-datepicker-toggle matSuffix [for]=\"manDate\"></mat-datepicker-toggle>\n      <mat-datepicker #manDate></mat-datepicker>\n    </mat-form-field>\n  </div>\n  <div class=\"form-group\">\n    <button class=\"btn btn-primary btn-lg btn-block pb_btn-pill btn-shadow-blue\">Save</button>\n  </div>\n</form>\n<div class=\"container success-cont\" [hidden]=\"!submittedLostItem\">\n  <div class=\"row align-items-center justify-content-center\">\n    <div class=\"col-md-3\"></div>\n    <div class=\"col-md-6 relative align-self-center\">\n      <img src=\"assets/images/green.png\" class=\"success-img\" />\n      <h1 class=\"mb-4 mt-0 text-center\">You have submitted an item!</h1>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/lostitem/lostitem.component.ts":
/*!************************************************!*\
  !*** ./src/app/lostitem/lostitem.component.ts ***!
  \************************************************/
/*! exports provided: LostitemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LostitemComponent", function() { return LostitemComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _models_user__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../models/user */ "./src/app/models/user.ts");
/* harmony import */ var _models_lostItem__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../models/lostItem */ "./src/app/models/lostItem.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");






var LostitemComponent = /** @class */ (function () {
    function LostitemComponent(userService, router) {
        this.userService = userService;
        this.router = router;
        this.submittedLostItem = false;
        this.model = new _models_user__WEBPACK_IMPORTED_MODULE_3__["User"]();
        this.lostItem = new _models_lostItem__WEBPACK_IMPORTED_MODULE_4__["LostItem"]();
        this.minManDate = new Date();
        this.maxManDate = new Date(2030, 0, 1);
        this.model = this.userService.currentUser;
    }
    LostitemComponent.prototype.ngOnInit = function () {
    };
    LostitemComponent.prototype.onSubmitLostItem = function () {
        var _this = this;
        this.userService.reportLostItem(this.lostItem).subscribe(function (result) {
            _this.submittedLostItem = true;
            setTimeout(function () {
                _this.submittedLostItem = false;
            }, 3000); //5s
        }, function (error) {
            _this.submittedLostItem = false;
            _this.router.navigate(['']);
        });
    };
    LostitemComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-lostitem',
            template: __webpack_require__(/*! ./lostitem.component.html */ "./src/app/lostitem/lostitem.component.html"),
            styles: [__webpack_require__(/*! ./lostitem.component.css */ "./src/app/lostitem/lostitem.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], LostitemComponent);
    return LostitemComponent;
}());



/***/ }),

/***/ "./src/app/main/main.component.css":
/*!*****************************************!*\
  !*** ./src/app/main/main.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#map {\r\n    height: 100%;\r\n}\r\n\r\nhtml,\r\nbody {\r\n    height: 100%;\r\n    margin: 0;\r\n    padding: 0;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWFpbi9tYWluLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxZQUFZO0FBQ2hCOztBQUVBOztJQUVJLFlBQVk7SUFDWixTQUFTO0lBQ1QsVUFBVTtBQUNkIiwiZmlsZSI6InNyYy9hcHAvbWFpbi9tYWluLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjbWFwIHtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG5cclxuaHRtbCxcclxuYm9keSB7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/main/main.component.html":
/*!******************************************!*\
  !*** ./src/app/main/main.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<agm-map style=\"height: 100%\" [latitude]=\"lat\" [longitude]=\"lng\" [styles]=\"styles\" [zoom]=\"zoom\">\n  <agm-marker [latitude]=\"lat\" [longitude]=\"lng\" [iconUrl]=\"icon\">\n  </agm-marker>\n  <div *ngIf=\"navigationLinks\">\n    <agm-direction [origin]=\"origin\" [destination]=\"destination\">\n    </agm-direction>\n  </div>\n</agm-map>\n"

/***/ }),

/***/ "./src/app/main/main.component.ts":
/*!****************************************!*\
  !*** ./src/app/main/main.component.ts ***!
  \****************************************/
/*! exports provided: MainComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainComponent", function() { return MainComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_location_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/location.service */ "./src/app/services/location.service.ts");
/* harmony import */ var _services_menu_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/menu.service */ "./src/app/services/menu.service.ts");
/* harmony import */ var ngx_logger__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-logger */ "./node_modules/ngx-logger/esm5/ngx-logger.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");






var MainComponent = /** @class */ (function () {
    function MainComponent(locationService, menuService, logger, router) {
        this.locationService = locationService;
        this.menuService = menuService;
        this.logger = logger;
        this.router = router;
        this.zoom = 15;
        this.icon = {
            url: './assets/images/pointer.png',
            scaledSize: {
                width: 40,
                height: 40
            }
        };
        this.styles = [{
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [{
                        "color": "#e9e9e9"
                    },
                    {
                        "lightness": 17
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "geometry",
                "stylers": [{
                        "color": "#f5f5f5"
                    },
                    {
                        "lightness": 20
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [{
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 17
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [{
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 29
                    },
                    {
                        "weight": 0.2
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [{
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 18
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "geometry",
                "stylers": [{
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 16
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [{
                        "color": "#f5f5f5"
                    },
                    {
                        "lightness": 21
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [{
                        "color": "#dedede"
                    },
                    {
                        "lightness": 21
                    }
                ]
            },
            {
                "elementType": "labels.text.stroke",
                "stylers": [{
                        "visibility": "on"
                    },
                    {
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 16
                    }
                ]
            },
            {
                "elementType": "labels.text.fill",
                "stylers": [{
                        "saturation": 36
                    },
                    {
                        "color": "#333333"
                    },
                    {
                        "lightness": 40
                    }
                ]
            },
            {
                "elementType": "labels.icon",
                "stylers": [{
                        "visibility": "off"
                    }]
            },
            {
                "featureType": "transit",
                "elementType": "geometry",
                "stylers": [{
                        "color": "#f2f2f2"
                    },
                    {
                        "lightness": 19
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.fill",
                "stylers": [{
                        "color": "#fefefe"
                    },
                    {
                        "lightness": 20
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [{
                        "color": "#fefefe"
                    },
                    {
                        "lightness": 17
                    },
                    {
                        "weight": 1.2
                    }
                ]
            }
        ];
    }
    MainComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.locationService.navigationStatus.subscribe(function (links) {
            _this.logger.debug("MainComponent: Received navigation coordinates from locationService.navigationStatus: " + JSON.stringify(links));
            _this.navigationLinks = links;
            if (links) {
                _this.origin = { lat: Number(links.fromLat), lng: Number(links.fromLong) };
                _this.destination = { lat: Number(links.whereLat), lng: Number(links.whereLong) };
                _this.logger.debug("MainComponent: Set navigation links to " + JSON.stringify(_this.origin) + " and " + JSON.stringify(_this.destination));
            }
            // this.locationService.setCurrentPosition();
        }, function (error) {
            _this.router.navigate(['']);
            _this.logger.error("MainComponent: Error receiving navigation coordinates");
            console.log(error);
        });
        this.locationService.setCurrentPosition();
        this.locationService.currentCoords.subscribe(function (coords) {
            _this.logger.debug("MainComponent: Received current coordinates from locationService.currentCoords: " + JSON.stringify(coords));
            if (coords.whereLat && coords.whereLong) {
                _this.lat = coords.whereLat;
                _this.lng = coords.whereLong;
            }
            else if (coords.fromLat, coords.fromLong) {
                _this.lat = coords.fromLat;
                _this.lng = coords.fromLong;
            }
        }, function (error) {
            _this.router.navigate(['']);
            _this.logger.error("MainComponent: Error receiving current coordinates");
            console.log(error);
        });
    };
    MainComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-main',
            template: __webpack_require__(/*! ./main.component.html */ "./src/app/main/main.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./main.component.css */ "./src/app/main/main.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_location_service__WEBPACK_IMPORTED_MODULE_2__["LocationService"], _services_menu_service__WEBPACK_IMPORTED_MODULE_3__["MenuService"], ngx_logger__WEBPACK_IMPORTED_MODULE_4__["NGXLogger"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], MainComponent);
    return MainComponent;
}());



/***/ }),

/***/ "./src/app/material.module.ts":
/*!************************************!*\
  !*** ./src/app/material.module.ts ***!
  \************************************/
/*! exports provided: MaterialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaterialModule", function() { return MaterialModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");




var MaterialModule = /** @class */ (function () {
    function MaterialModule() {
    }
    MaterialModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatProgressSpinnerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__["BrowserAnimationsModule"],
            ],
            exports: [
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatProgressSpinnerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__["BrowserAnimationsModule"]
            ],
            providers: [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDatepickerModule"]],
        })
    ], MaterialModule);
    return MaterialModule;
}());



/***/ }),

/***/ "./src/app/menu/menu.component.css":
/*!*****************************************!*\
  !*** ./src/app/menu/menu.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".search-bar {\r\n  background-color: aliceblue;\r\n  text-align: center;\r\n}\r\n\r\n.profile-icon {\r\n  height: 30px;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWVudS9tZW51LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSwyQkFBMkI7RUFDM0Isa0JBQWtCO0FBQ3BCOztBQUVBO0VBQ0UsWUFBWTtBQUNkIiwiZmlsZSI6InNyYy9hcHAvbWVudS9tZW51LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc2VhcmNoLWJhciB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogYWxpY2VibHVlO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuLnByb2ZpbGUtaWNvbiB7XHJcbiAgaGVpZ2h0OiAzMHB4O1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/menu/menu.component.html":
/*!******************************************!*\
  !*** ./src/app/menu/menu.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-md navbar-dark blue-bg\">\n  <a class=\"navbar-brand\" href=\"index.html\">\n    <img src=\"assets/images/android-chrome-192x192.png\" class=\"logo\" /> </a>\n  <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbar5\">\n    <span class=\"navbar-toggler-icon\"></span>\n  </button>\n  <div class=\"navbar-collapse collapse\" id=\"navbar5\">\n\n    <mat-form-field class=\"mx-2 my-auto d-inline w-75\" *ngIf=\"loggedIn\">\n      <div class=\"input-group\">\n        <input matInput matGoogleMapsAutocomplete (onLocationSelected)=\"setDestinationCoordinates($event)\"\n          [(ngModel)]=\"inputMessage\" type=\"text\" class=\"form-control search-bar\">\n      </div>\n    </mat-form-field>\n\n    <ul class=\"navbar-nav nav-adj\" *ngIf=\"!loggedIn\">\n      <li class=\"nav-item\" *ngIf=\"!loggedIn\"><a class=\"nav-link\" href=\"/#section-home\">Home</a></li>\n      <li class=\"nav-item\" *ngIf=\"!loggedIn\"><a class=\"nav-link\" href=\"/#section-pricing\">Pricing</a></li>\n      <li class=\"nav-item\" *ngIf=\"!loggedIn\"><a class=\"nav-link\" href=\"/#section-features\">Features</a></li>\n      <li class=\"nav-item\" *ngIf=\"!loggedIn\"><a class=\"nav-link\" routerLink=\"register\"\n          routerLinkActive=\"active\">Register</a></li>\n    </ul>\n    <ul class=\"navbar-nav\" *ngIf=\"loggedIn\">\n\n      <li class=\"nav-item\" *ngIf=\"loggedIn && menu === 'profile'\"><a class=\"nav-link\" [routerLink]=\"\"\n          (click)=\"returnToMain()\">Back to the main page</a></li>\n      <li class=\"nav-item\" *ngIf=\"links.getOpen && menu === 'main'\"><a class=\"nav-link\" [routerLink]=\"\"\n          (click)=\"executeAction('getOpen')\">See open orders</a></li>\n      <li class=\"nav-item\" *ngIf=\"links.cancel && menu === 'main'\"><a class=\"nav-link\" [routerLink]=\"\"\n          (click)=\"executeAction('cancel')\">Cancel</a></li>\n      <!-- <li class=\"nav-item\" *ngIf=\"links.accept && menu === 'main'\"><a class=\"nav-link\" [routerLink]=\"\"\n          (click)=\"executeAction('accept')\">Accept</a></li> -->\n      <li class=\"nav-item\" *ngIf=\"links.start && menu === 'main'\"><a class=\"nav-link\" [routerLink]=\"\"\n          (click)=\"executeAction('start')\">Start</a></li>\n      <li class=\"nav-item\" *ngIf=\"links.terminate && menu === 'main'\"><a class=\"nav-link\" [routerLink]=\"\"\n          (click)=\"executeAction('terminate')\">Terminate</a></li>\n      <li class=\"nav-item\" *ngIf=\"links.complete && menu === 'main'\"><a class=\"nav-link\" [routerLink]=\"\"\n          (click)=\"executeAction('complete')\">Complete</a></li>\n      <li class=\"nav-item\" *ngIf=\"links.payment && menu === 'main'\"><a class=\"nav-link\" [routerLink]=\"\"\n          (click)=\"executeAction('payment')\">Pay</a></li>\n      <li class=\"nav-item\" *ngIf=\"links.create && menu === 'main'\"><a class=\"nav-link\" [routerLink]=\"\"\n          (click)=\"executeAction('create')\">Order a taxi</a> </li>\n      <li class=\"nav-item\" *ngIf=\"loggedIn\"> <a class=\"nav-link\" (click)=\"logout()\" href=\"\">Logout</a></li>\n      <li class=\"nav-item\" *ngIf=\"loggedIn && menu === 'main'\">\n        <a class=\"nav-link\" [routerLink]=\"\" (click)=\"openProfilePage()\">\n          <img src=\"assets/images/ico.png\" class=\"profile-icon\" /> </a>\n      </li>\n    </ul>\n\n  </div>\n</nav>\n"

/***/ }),

/***/ "./src/app/menu/menu.component.ts":
/*!****************************************!*\
  !*** ./src/app/menu/menu.component.ts ***!
  \****************************************/
/*! exports provided: MenuComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuComponent", function() { return MenuComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_order_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/order.service */ "./src/app/services/order.service.ts");
/* harmony import */ var _services_location_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/location.service */ "./src/app/services/location.service.ts");
/* harmony import */ var _models_link__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../models/link */ "./src/app/models/link.ts");
/* harmony import */ var _models_coordinates__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../models/coordinates */ "./src/app/models/coordinates.ts");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _services_menu_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../services/menu.service */ "./src/app/services/menu.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var ngx_logger__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-logger */ "./node_modules/ngx-logger/esm5/ngx-logger.js");
/* harmony import */ var _services_message_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../services/message.service */ "./src/app/services/message.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");












var MAX_DISTANCE = 30;
var MenuComponent = /** @class */ (function () {
    function MenuComponent(orderService, locationService, userService, menuService, messageService, logger, router, dialog) {
        this.orderService = orderService;
        this.locationService = locationService;
        this.userService = userService;
        this.menuService = menuService;
        this.messageService = messageService;
        this.logger = logger;
        this.router = router;
        this.dialog = dialog;
        this.links = new _models_link__WEBPACK_IMPORTED_MODULE_4__["Link"]();
        this.loggedIn = false;
        this.validDestination = true;
        this.coordinates = new _models_coordinates__WEBPACK_IMPORTED_MODULE_5__["Coordinates"]();
    }
    MenuComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.userService.authStatus.subscribe(function (status) {
            _this.logger.debug("MenuComponent: Received update from userService.authStatus: " + JSON.stringify(status));
            _this.loggedIn = status;
        });
        this.userService.userStatus.subscribe(function (user) {
            _this.logger.debug("MenuComponent: Received update from userService.userStatus: " + JSON.stringify(user));
            _this.currentUser = user;
        });
        this.menuService.linksStatus.subscribe(function (links) {
            _this.logger.debug("MenuComponent: Received update from menuService.linksStatus: " + JSON.stringify(links));
            _this.links = links;
        });
        this.locationService.currentCoords.subscribe(function (coords) {
            _this.logger.debug("MenuComponent: Received update from locationService.currentCoords: " + JSON.stringify(coords));
            _this.coordinates = coords;
        });
        this.orderService.orderStatus.subscribe(function (order) {
            _this.logger.debug("MenuComponent: Received update from orderService.orderStatus: " + JSON.stringify(order));
            _this.menuService.lastOrder = order;
        });
        this.menu = "main";
    };
    MenuComponent.prototype.logout = function () {
        // this.webSocketService.disconnect();
    };
    MenuComponent.prototype.updateCoords = function () {
        this.logger.debug("MenuComponent: Updating coordinates in locationService with: " + this.coordinates);
        this.locationService.changeCoordinates(this.coordinates);
    };
    MenuComponent.prototype.executeAction = function (action) {
        var _this = this;
        this.logger.debug("MenuComponent: Executing action: " + action);
        if (action === 'create' && !this.checkDestinationCoordinates()) {
            this.logger.debug("MenuComponent: Selected destination is not valid");
            this.locationService.resetDestinationCoordinates();
        }
        else if (action === 'getOpen') {
            this.generateAllOpenOrdersPopup();
        }
        else {
            var costEstimation = this.orderService.calculateCost(this.locationService.calculateDistance(this.coordinates));
            if (this.currentUser.driver === null && action === "cancel") {
                costEstimation = 0;
            }
            this.messageService.showConfirmationPopup(action, costEstimation, this.menuService.lastOrder).subscribe(function (confirmation) {
                _this.logger.debug("MenuComponent: Popup has been closed, received object: " + JSON.stringify(confirmation));
                if (confirmation) {
                    _this.menuService.executeAction(action);
                }
            }, function (error) {
                _this.logger.debug("MenuComponent: Error getting message result: " + JSON.stringify(error));
                _this.router.navigate(['']);
            });
        }
        this.cleanUpInputField();
    };
    MenuComponent.prototype.cleanUpInputField = function () {
        this.inputMessage = "";
    };
    MenuComponent.prototype.openProfilePage = function () {
        this.menu = 'profile';
        this.router.navigate(['/profile']);
    };
    MenuComponent.prototype.returnToMain = function () {
        this.menu = 'main';
        this.router.navigate(['/main']);
    };
    MenuComponent.prototype.destinationIsTooFar = function () {
        var valid;
        var distance = this.locationService.calculateDistance(this.coordinates);
        if (distance > MAX_DISTANCE) {
            valid = true;
        }
        else {
            this.updateCoords();
            valid = false;
        }
        this.logger.debug("MenuComponent: Selected destination is valid: " + this.validDestination);
        return valid;
    };
    MenuComponent.prototype.destinationCoordsNotExist = function () {
        if (!this.coordinates.whereLat || !this.coordinates.whereLong) {
            return true;
        }
        else
            return false;
    };
    MenuComponent.prototype.generateAllOpenOrdersPopup = function () {
        var _this = this;
        this.menuService.getListOfOpenOrders().subscribe(function (orders) {
            _this.logger.debug("MenuComponent: Received list of open orders " + JSON.stringify(orders));
            if (orders._embedded) {
                _this.logger.debug("MenuComponent: Orders._embedded equals to " + JSON.stringify(orders._embedded));
                _this.messageService.showAllOpenOrdersPopup(orders)
                    .subscribe(function (selectedOrder) {
                    if (selectedOrder) {
                        _this.acceptOrder(selectedOrder);
                        _this.logger.debug("MenuComponent: Popup has been closed, accepted order: " + JSON.stringify(selectedOrder));
                    }
                }, function (error) {
                    _this.logger.error("MenuComponent: Error getting message result: " + JSON.stringify(error));
                    _this.router.navigate(['']);
                });
            }
        }, function (error) {
            _this.logger.error("MenuComponent: Error getting list of orders: " + JSON.stringify(error));
            _this.router.navigate(['']);
        });
    };
    MenuComponent.prototype.acceptOrder = function (selectedOrder) {
        var _this = this;
        this.orderService.getOrderById(selectedOrder.orderId).subscribe(function (order) {
            _this.logger.debug("MenuComponent: requested order by id, received: " + JSON.stringify(order));
            _this.menuService.updateLinks(order);
            _this.menuService.executeAction('accept');
        }, function (error) {
            _this.logger.error("MenuComponent: Error getting order by id: " + JSON.stringify(error));
            _this.router.navigate(['']);
        });
    };
    MenuComponent.prototype.setDestinationCoordinates = function (location) {
        this.coordinates.whereLat = location.latitude;
        this.coordinates.whereLong = location.longitude;
    };
    MenuComponent.prototype.checkDestinationCoordinates = function () {
        var valid = false;
        if (this.destinationCoordsNotExist()) {
            this.messageService.showErrorPopUp("Please choose your destination address");
        }
        else if (this.destinationIsTooFar()) {
            this.messageService.showErrorPopUp("Sorry, but the destination is too far from your current location");
        }
        else {
            this.logger.debug("MenuComponent: Set destination coordinates to: " + JSON.stringify(location));
            valid = true;
        }
        return valid;
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], MenuComponent.prototype, "action", void 0);
    MenuComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-menu',
            template: __webpack_require__(/*! ./menu.component.html */ "./src/app/menu/menu.component.html"),
            styles: [__webpack_require__(/*! ./menu.component.css */ "./src/app/menu/menu.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_order_service__WEBPACK_IMPORTED_MODULE_2__["OrderService"],
            _services_location_service__WEBPACK_IMPORTED_MODULE_3__["LocationService"],
            _services_user_service__WEBPACK_IMPORTED_MODULE_6__["UserService"],
            _services_menu_service__WEBPACK_IMPORTED_MODULE_7__["MenuService"],
            _services_message_service__WEBPACK_IMPORTED_MODULE_10__["MessageService"],
            ngx_logger__WEBPACK_IMPORTED_MODULE_9__["NGXLogger"],
            _angular_router__WEBPACK_IMPORTED_MODULE_11__["Router"],
            _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatDialog"]])
    ], MenuComponent);
    return MenuComponent;
}());



/***/ }),

/***/ "./src/app/message/message.component.css":
/*!***********************************************!*\
  !*** ./src/app/message/message.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* * {\r\n  margin: 0;\r\n  padding: 0;\r\n} */\r\n\r\n.rate {\r\n  float: left;\r\n  height: 46px;\r\n  padding: 0 10px;\r\n  margin: 0;\r\n  padding: 0;\r\n}\r\n\r\n.rate:not(:checked)>input {\r\n  position: absolute;\r\n  top: -9999px;\r\n}\r\n\r\n.rate:not(:checked)>label {\r\n  float: right;\r\n  width: 1em;\r\n  overflow: hidden;\r\n  white-space: nowrap;\r\n  cursor: pointer;\r\n  font-size: 30px;\r\n  color: #ccc;\r\n}\r\n\r\n.rate:not(:checked)>label:before {\r\n  content: '★ ';\r\n}\r\n\r\n.rate>input:checked~label {\r\n  color: #ffc700;\r\n}\r\n\r\n.rate:not(:checked)>label:hover,\r\n.rate:not(:checked)>label:hover~label {\r\n  color: #deb217;\r\n}\r\n\r\n.rate>input:checked+label:hover,\r\n.rate>input:checked+label:hover~label,\r\n.rate>input:checked~label:hover,\r\n.rate>input:checked~label:hover~label,\r\n.rate>label:hover~input:checked~label {\r\n  color: #c59b08;\r\n}\r\n\r\n.profile-img {\r\n  border-radius: 50%;\r\n  -o-object-fit: cover;\r\n     object-fit: cover;\r\n  height: 100px;\r\n  width: 100px;\r\n}\r\n\r\n.star-rating {\r\n  left: 50%;\r\n  position: absolute;\r\n  -webkit-transform: translate(-50%, -50%);\r\n          transform: translate(-50%, -50%);\r\n  margin-top: 1%;\r\n}\r\n\r\n.summary {\r\n  margin-left: 10px;\r\n  margin-bottom: 0px;\r\n}\r\n\r\n.summary-image {\r\n  margin-bottom: 30px;\r\n  width: 300px\r\n}\r\n\r\n.star-rating-center {\r\n  text-align: center\r\n}\r\n\r\n.notif-button {\r\n  margin: auto;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWVzc2FnZS9tZXNzYWdlLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7OztHQUdHOztBQUVIO0VBQ0UsV0FBVztFQUNYLFlBQVk7RUFDWixlQUFlO0VBQ2YsU0FBUztFQUNULFVBQVU7QUFDWjs7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixZQUFZO0FBQ2Q7O0FBRUE7RUFDRSxZQUFZO0VBQ1osVUFBVTtFQUNWLGdCQUFnQjtFQUNoQixtQkFBbUI7RUFDbkIsZUFBZTtFQUNmLGVBQWU7RUFDZixXQUFXO0FBQ2I7O0FBRUE7RUFDRSxhQUFhO0FBQ2Y7O0FBRUE7RUFDRSxjQUFjO0FBQ2hCOztBQUVBOztFQUVFLGNBQWM7QUFDaEI7O0FBRUE7Ozs7O0VBS0UsY0FBYztBQUNoQjs7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixvQkFBaUI7S0FBakIsaUJBQWlCO0VBQ2pCLGFBQWE7RUFDYixZQUFZO0FBQ2Q7O0FBRUE7RUFDRSxTQUFTO0VBQ1Qsa0JBQWtCO0VBQ2xCLHdDQUFnQztVQUFoQyxnQ0FBZ0M7RUFDaEMsY0FBYztBQUNoQjs7QUFFQTtFQUNFLGlCQUFpQjtFQUNqQixrQkFBa0I7QUFDcEI7O0FBRUE7RUFDRSxtQkFBbUI7RUFDbkI7QUFDRjs7QUFFQTtFQUNFO0FBQ0Y7O0FBRUE7RUFDRSxZQUFZO0FBQ2QiLCJmaWxlIjoic3JjL2FwcC9tZXNzYWdlL21lc3NhZ2UuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qICoge1xyXG4gIG1hcmdpbjogMDtcclxuICBwYWRkaW5nOiAwO1xyXG59ICovXHJcblxyXG4ucmF0ZSB7XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbiAgaGVpZ2h0OiA0NnB4O1xyXG4gIHBhZGRpbmc6IDAgMTBweDtcclxuICBtYXJnaW46IDA7XHJcbiAgcGFkZGluZzogMDtcclxufVxyXG5cclxuLnJhdGU6bm90KDpjaGVja2VkKT5pbnB1dCB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogLTk5OTlweDtcclxufVxyXG5cclxuLnJhdGU6bm90KDpjaGVja2VkKT5sYWJlbCB7XHJcbiAgZmxvYXQ6IHJpZ2h0O1xyXG4gIHdpZHRoOiAxZW07XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuICBmb250LXNpemU6IDMwcHg7XHJcbiAgY29sb3I6ICNjY2M7XHJcbn1cclxuXHJcbi5yYXRlOm5vdCg6Y2hlY2tlZCk+bGFiZWw6YmVmb3JlIHtcclxuICBjb250ZW50OiAn4piFICc7XHJcbn1cclxuXHJcbi5yYXRlPmlucHV0OmNoZWNrZWR+bGFiZWwge1xyXG4gIGNvbG9yOiAjZmZjNzAwO1xyXG59XHJcblxyXG4ucmF0ZTpub3QoOmNoZWNrZWQpPmxhYmVsOmhvdmVyLFxyXG4ucmF0ZTpub3QoOmNoZWNrZWQpPmxhYmVsOmhvdmVyfmxhYmVsIHtcclxuICBjb2xvcjogI2RlYjIxNztcclxufVxyXG5cclxuLnJhdGU+aW5wdXQ6Y2hlY2tlZCtsYWJlbDpob3ZlcixcclxuLnJhdGU+aW5wdXQ6Y2hlY2tlZCtsYWJlbDpob3Zlcn5sYWJlbCxcclxuLnJhdGU+aW5wdXQ6Y2hlY2tlZH5sYWJlbDpob3ZlcixcclxuLnJhdGU+aW5wdXQ6Y2hlY2tlZH5sYWJlbDpob3Zlcn5sYWJlbCxcclxuLnJhdGU+bGFiZWw6aG92ZXJ+aW5wdXQ6Y2hlY2tlZH5sYWJlbCB7XHJcbiAgY29sb3I6ICNjNTliMDg7XHJcbn1cclxuXHJcbi5wcm9maWxlLWltZyB7XHJcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gIG9iamVjdC1maXQ6IGNvdmVyO1xyXG4gIGhlaWdodDogMTAwcHg7XHJcbiAgd2lkdGg6IDEwMHB4O1xyXG59XHJcblxyXG4uc3Rhci1yYXRpbmcge1xyXG4gIGxlZnQ6IDUwJTtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XHJcbiAgbWFyZ2luLXRvcDogMSU7XHJcbn1cclxuXHJcbi5zdW1tYXJ5IHtcclxuICBtYXJnaW4tbGVmdDogMTBweDtcclxuICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbn1cclxuXHJcbi5zdW1tYXJ5LWltYWdlIHtcclxuICBtYXJnaW4tYm90dG9tOiAzMHB4O1xyXG4gIHdpZHRoOiAzMDBweFxyXG59XHJcblxyXG4uc3Rhci1yYXRpbmctY2VudGVyIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXJcclxufVxyXG5cclxuLm5vdGlmLWJ1dHRvbiB7XHJcbiAgbWFyZ2luOiBhdXRvO1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/message/message.component.html":
/*!************************************************!*\
  !*** ./src/app/message/message.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"type==='info'\">\n  <h1 mat-dialog-title>{{ messageTitle }}</h1>\n  <div mat-dialog-content>\n    <p>{{ messageText }}</p>\n  </div>\n  <div mat-dialog-actions>\n    <button mat-button (click)=\"onClickClose()\" class=\"notif-button\">OK</button>\n  </div>\n</div>\n\n<div *ngIf=\"type==='accept'\">\n  <h1 mat-dialog-title>\n    <img class=\"card-img-top profile-img\" src=\"{{imageUrl}}\" alt=\"Card image\" *ngIf=\"imageUrl\">\n    {{ messageTitle }}</h1>\n  <div mat-dialog-content>\n    <p>{{ messageText }}</p>\n  </div>\n  <div mat-dialog-actions>\n    <button mat-button (click)=\"onClickClose()\" class=\"notif-button\">OK</button>\n  </div>\n</div>\n\n<div *ngIf=\"type==='confirmation'\">\n  <h1 mat-dialog-title>{{ messageTitle }}</h1>\n  <div mat-dialog-content>\n    <p>{{ messageText }}</p>\n  </div>\n  <div mat-dialog-actions>\n    <button mat-button (click)=\"onClickConfirm()\" cdkFocusInitial class=\"notif-button\">Confirm</button>\n    <button mat-button (click)=\"onClickClose()\" class=\"notif-button\">No Thanks</button>\n  </div>\n</div>\n\n<div *ngIf=\"type==='error'\">\n  <h1 mat-dialog-title>{{ messageTitle }}</h1>\n  <div mat-dialog-actions>\n    <button mat-button (click)=\"onClickClose()\" class=\"notif-button\">OK</button>\n  </div>\n</div>\n\n<div *ngIf=\"type==='all orders'\">\n  <h1 mat-dialog-title> Currently open orders </h1>\n  <mat-action-list>\n    <a mat-list-item (click)=\"acceptOrder(order)\" *ngFor=\"let order of orders\">Client waiting at\n      {{ order.fromName }}</a>\n  </mat-action-list>\n</div>\n\n<div class=\"card\" *ngIf=\"type==='summary'\">\n  <img src=\"assets/images/taxi.JPG\" class=\"card-img-top summary-image\" />\n  <br>\n  <mat-action-list>\n    <p class=\"summary\" *ngIf=\"order.date\">Date: {{ order.date.substring(0,10)}}</p>\n    <p class=\"summary\" *ngIf=\"order.fromName\">From: {{ order.fromName }}</p>\n    <p class=\"summary\" *ngIf=\"order.whereName\">Where: {{ order.whereName }}</p>\n    <p class=\"summary\" *ngIf=\"order.status\">Status: {{ order.status }}</p>\n    <p class=\"summary\" *ngIf=\"order.time\">Time: {{ order.time/1000 }}s</p>\n    <p class=\"summary\" *ngIf=\"order.rating\">Feedback: {{ rating[order.rating] }}</p>\n    <p class=\"summary\" *ngIf=\"order.distance\">Distance: {{ (order.distance).toFixed(2) }}km</p>\n    <p class=\"summary\" *ngIf=\"order.cost\">Cost: {{ (order.cost).toFixed(2) }}EUR</p>\n  </mat-action-list>\n  <br>\n  <button mat-button (click)=\"onClickClose()\" class=\"notif-button\" style=\"margin-top: 10px\">OK</button>\n</div>\n\n\n\n\n<div class=\"card\" *ngIf=\"type==='summary-passenger'\">\n  <img src=\"assets/images/taxi.JPG\" class=\"card-img-top summary-image\" />\n  <br>\n  <mat-action-list>\n    <p class=\"summary\" *ngIf=\"order.date\">Date: {{ order.date.substring(0,10)}}</p>\n    <p class=\"summary\" *ngIf=\"order.fromName\">From: {{ order.fromName }}</p>\n    <p class=\"summary\" *ngIf=\"order.whereName\">Where: {{ order.whereName }}</p>\n    <p class=\"summary\" *ngIf=\"order.status\">Status: {{ order.status }}</p>\n    <p class=\"summary\" *ngIf=\"order.time\">Time: {{ order.time/1000 }}s</p>\n    <p class=\"summary\" *ngIf=\"order.rating\">Feedback: {{ rating[order.rating] }}</p>\n    <p class=\"summary\" *ngIf=\"order.distance\">Distance: {{ (order.distance).toFixed(2) }}km</p>\n    <p class=\"summary\" *ngIf=\"order.cost\">Cost: {{ (order.cost).toFixed(2) }}EUR</p>\n  </mat-action-list>\n  <br>\n\n  <div class=\"star-rating-center\">\n    <h2 style=\"margin-bottom: 30px\">\n      Rate your ride!\n    </h2>\n    <div class=\"rate star-rating\">\n      <input type=\"radio\" id=\"star5\" name=\"rate\" value=\"5\" (click)=\"onSetRating(5)\" />\n      <label for=\"star5\" title=\"text\">5 stars</label>\n      <input type=\"radio\" id=\"star4\" name=\"rate\" value=\"4\" (click)=\"onSetRating(4)\" />\n      <label for=\"star4\" title=\"text\">4 stars</label>\n      <input type=\"radio\" id=\"star3\" name=\"rate\" value=\"3\" (click)=\"onSetRating(3)\" />\n      <label for=\"star3\" title=\"text\">3 stars</label>\n      <input type=\"radio\" id=\"star2\" name=\"rate\" value=\"2\" (click)=\"onSetRating(2)\" />\n      <label for=\"star2\" title=\"text\">2 stars</label>\n      <input type=\"radio\" id=\"star1\" name=\"rate\" value=\"1\" (click)=\"onSetRating(1)\" />\n      <label for=\"star1\" title=\"text\">1 star</label>\n    </div>\n    <br>\n  </div>\n\n</div>\n\n\n\n<!-- <div *ngIf=\"type==='summary-passenger'\"> -->\n<!-- <h1 mat-dialog-title> Here is a summary of your ride </!-->\n<!-- <mat-action-list>\n    <li>From: {{ order.fromName }}</li>\n    <li>Where: {{ order.whereName }}</li>\n    <li>Status: {{ order.status }}</li>\n    <li>Time: {{ order.time/1000 }}</li>\n    <li>Distance: {{ (order.distance).toFixed(2) }}</li>\n    <li>Cost: {{ (order.cost).toFixed(2) }}</li>\n  </mat-action-list> -->\n\n<!-- <br> -->\n<!-- <div class=\"star-rating-center\">\n    <h2>\n      Rate your ride!\n    </h2>\n    <div class=\"rate star-rating\">\n      <input type=\"radio\" id=\"star5\" name=\"rate\" value=\"5\" (click)=\"onSetRating(5)\" />\n      <label for=\"star5\" title=\"text\">5 stars</label>\n      <input type=\"radio\" id=\"star4\" name=\"rate\" value=\"4\" (click)=\"onSetRating(4)\" />\n      <label for=\"star4\" title=\"text\">4 stars</label>\n      <input type=\"radio\" id=\"star3\" name=\"rate\" value=\"3\" (click)=\"onSetRating(3)\" />\n      <label for=\"star3\" title=\"text\">3 stars</label>\n      <input type=\"radio\" id=\"star2\" name=\"rate\" value=\"2\" (click)=\"onSetRating(2)\" />\n      <label for=\"star2\" title=\"text\">2 stars</label>\n      <input type=\"radio\" id=\"star1\" name=\"rate\" value=\"1\" (click)=\"onSetRating(1)\" />\n      <label for=\"star1\" title=\"text\">1 star</label>\n    </div>\n    <br>\n  </div>\n  <br>\n</div> -->\n"

/***/ }),

/***/ "./src/app/message/message.component.ts":
/*!**********************************************!*\
  !*** ./src/app/message/message.component.ts ***!
  \**********************************************/
/*! exports provided: MessageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MessageComponent", function() { return MessageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var ngx_logger__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-logger */ "./node_modules/ngx-logger/esm5/ngx-logger.js");




var MessageComponent = /** @class */ (function () {
    function MessageComponent(dialogRef, data, logger) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.logger = logger;
        this.rating = ["TERRIBLE", "DISAPPOINTMENT", "OK", "GOOD", "PERFECT"];
    }
    MessageComponent.prototype.ngOnInit = function () {
        this.type = this.data.type;
        switch (this.data.type) {
            case "all orders":
                this.orders = this.data.orders;
                break;
            case "error":
                this.messageTitle = this.data.info;
                break;
            case "accept":
                this.messageTitle = this.data.title;
                this.messageText = this.data.info;
                this.order = this.data.order;
                if (this.data.order.driver.driver.driversPhoto) {
                    this.imageUrl = "http://localhost:8080/user/downloadDriverPhoto/" + this.data.order.driver.driver.driversPhoto;
                }
                break;
            default:
                this.messageTitle = this.data.title;
                this.messageText = this.data.info;
                this.order = this.data.order;
                break;
        }
    };
    // window.addEventListener("beforeunload", function() { debugger; }, false)
    MessageComponent.prototype.acceptOrder = function (order) {
        this.dialogRef.close(order);
        this.logger.debug("MessageComponent: accepted order: " + JSON.stringify(order));
    };
    MessageComponent.prototype.onClickClose = function () {
        this.dialogRef.close();
        this.logger.debug("MessageComponent: closed popup");
    };
    MessageComponent.prototype.onClickConfirm = function () {
        this.dialogRef.close(true);
        this.logger.debug("MessageComponent: popup confirmation received");
    };
    // onRate($event: { oldValue: number, newValue: number, starRating: StarRatingComponent }) {
    //   this.dialogRef.close($event.newValue - 1);
    // }
    MessageComponent.prototype.onSetRating = function (rating) {
        this.dialogRef.close(rating - 1);
        this.logger.debug("MessageComponent: closed popup");
    };
    MessageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-message',
            template: __webpack_require__(/*! ./message.component.html */ "./src/app/message/message.component.html"),
            styles: [__webpack_require__(/*! ./message.component.css */ "./src/app/message/message.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"], Object, ngx_logger__WEBPACK_IMPORTED_MODULE_3__["NGXLogger"]])
    ], MessageComponent);
    return MessageComponent;
}());



/***/ }),

/***/ "./src/app/models/coordinates.ts":
/*!***************************************!*\
  !*** ./src/app/models/coordinates.ts ***!
  \***************************************/
/*! exports provided: Coordinates */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Coordinates", function() { return Coordinates; });
var Coordinates = /** @class */ (function () {
    function Coordinates() {
    }
    return Coordinates;
}());



/***/ }),

/***/ "./src/app/models/link.ts":
/*!********************************!*\
  !*** ./src/app/models/link.ts ***!
  \********************************/
/*! exports provided: Link */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Link", function() { return Link; });
var Link = /** @class */ (function () {
    function Link() {
    }
    return Link;
}());



/***/ }),

/***/ "./src/app/models/lostItem.ts":
/*!************************************!*\
  !*** ./src/app/models/lostItem.ts ***!
  \************************************/
/*! exports provided: LostItem */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LostItem", function() { return LostItem; });
var LostItem = /** @class */ (function () {
    function LostItem() {
    }
    return LostItem;
}());



/***/ }),

/***/ "./src/app/models/order.ts":
/*!*********************************!*\
  !*** ./src/app/models/order.ts ***!
  \*********************************/
/*! exports provided: Order */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Order", function() { return Order; });
;
var Order = /** @class */ (function () {
    function Order() {
    }
    return Order;
}());



/***/ }),

/***/ "./src/app/models/user.ts":
/*!********************************!*\
  !*** ./src/app/models/user.ts ***!
  \********************************/
/*! exports provided: User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
var User = /** @class */ (function () {
    function User() {
    }
    return User;
}());



/***/ }),

/***/ "./src/app/order/order.component.css":
/*!*******************************************!*\
  !*** ./src/app/order/order.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL29yZGVyL29yZGVyLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/order/order.component.html":
/*!********************************************!*\
  !*** ./src/app/order/order.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n"

/***/ }),

/***/ "./src/app/order/order.component.ts":
/*!******************************************!*\
  !*** ./src/app/order/order.component.ts ***!
  \******************************************/
/*! exports provided: OrderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderComponent", function() { return OrderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_order_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/order.service */ "./src/app/services/order.service.ts");
/* harmony import */ var _models_order__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../models/order */ "./src/app/models/order.ts");
/* harmony import */ var ngx_logger__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-logger */ "./node_modules/ngx-logger/esm5/ngx-logger.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");






var OrderComponent = /** @class */ (function () {
    function OrderComponent(orderService, logger, router) {
        this.orderService = orderService;
        this.logger = logger;
        this.router = router;
        this.order = new _models_order__WEBPACK_IMPORTED_MODULE_3__["Order"]();
    }
    OrderComponent.prototype.getActiveOrder = function () {
        var _this = this;
        this.orderService.getActiveOrder().subscribe(function (order) {
            _this.order = order;
            _this.logger.debug("OrderComponent: received active order: " + JSON.stringify(order));
        }, function (error) {
            _this.logger.error("OrderComponent: error receiving an active order: " + JSON.stringify(error));
            _this.router.navigate(['']);
        });
    };
    OrderComponent.prototype.getOpenOrders = function () {
        var _this = this;
        this.orderService.getOpenOrders().subscribe(function (orders) {
            _this.orders = orders._embedded.orderDtoList;
            _this.logger.debug("OrderComponent: received open orders: " + JSON.stringify(orders));
        }, function (error) {
            _this.logger.error("OrderComponent: error receiving open orders: " + JSON.stringify(error));
            _this.router.navigate(['']);
        });
    };
    OrderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-order',
            template: __webpack_require__(/*! ./order.component.html */ "./src/app/order/order.component.html"),
            styles: [__webpack_require__(/*! ./order.component.css */ "./src/app/order/order.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_order_service__WEBPACK_IMPORTED_MODULE_2__["OrderService"], ngx_logger__WEBPACK_IMPORTED_MODULE_4__["NGXLogger"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], OrderComponent);
    return OrderComponent;
}());



/***/ }),

/***/ "./src/app/profile/profile.component.css":
/*!***********************************************!*\
  !*** ./src/app/profile/profile.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".profile-img-change {\r\n  width: 100%;\r\n  -o-object-fit: cover;\r\n     object-fit: cover;\r\n  height: 200px;\r\n  border-radius: 10%;\r\n}\r\n\r\n.profile-img-layout {\r\n  width: 200px;\r\n  margin-left: auto;\r\n  margin-right: auto;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZmlsZS9wcm9maWxlLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxXQUFXO0VBQ1gsb0JBQWlCO0tBQWpCLGlCQUFpQjtFQUNqQixhQUFhO0VBQ2Isa0JBQWtCO0FBQ3BCOztBQUVBO0VBQ0UsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixrQkFBa0I7QUFDcEIiLCJmaWxlIjoic3JjL2FwcC9wcm9maWxlL3Byb2ZpbGUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5wcm9maWxlLWltZy1jaGFuZ2Uge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIG9iamVjdC1maXQ6IGNvdmVyO1xyXG4gIGhlaWdodDogMjAwcHg7XHJcbiAgYm9yZGVyLXJhZGl1czogMTAlO1xyXG59XHJcblxyXG4ucHJvZmlsZS1pbWctbGF5b3V0IHtcclxuICB3aWR0aDogMjAwcHg7XHJcbiAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/profile/profile.component.html":
/*!************************************************!*\
  !*** ./src/app/profile/profile.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--Accordion wrapper-->\n<div class=\"accordion md-accordion\" id=\"accordionEx\" role=\"tablist\" aria-multiselectable=\"true\">\n\n  <div class=\"card\" *ngIf=\"model.driver\">\n    <div class=\"card-header\" role=\"tab\" id=\"headingZero\">\n      <a data-toggle=\"collapse\" data-parent=\"#accordionEx\" href=\"#collapseZero\" aria-expanded=\"true\"\n        aria-controls=\"collapseZero\">\n        <h5 class=\"mb-0\">\n          Add your profile picture\n        </h5>\n      </a>\n    </div>\n    <div id=\"collapseZero\" class=\"collapse\" role=\"tabpanel\" aria-labelledby=\"headingZero\" data-parent=\"#accordionEx\">\n      <div class=\"card-body\">\n        <div class=\"row\">\n          <div class=\"col-sm-3\">\n            <div class=\"profile-img-layout\">\n              <img class=\"card-img-top profile-img-change\" src=\"{{imageUrl}}\" alt=\"Profile picture\">\n            </div>\n          </div>\n          <div class=\"col-sm-3\">\n            <h4 class=\"card-title\">{{model.firstname}} {{model.lastname}}</h4>\n            <input style=\"display: none\" type=\"file\" (change)=\"onFileChanged($event)\" #fileInput>\n            <button class=\"btn btn-primary btn-block pb_btn-pill btn-shadow-blue\" (click)=\"fileInput.click()\">Select\n              File</button>\n            <button class=\"btn btn-primary btn-block pb_btn-pill btn-shadow-blue\" (click)=\"onUpload()\">Upload</button>\n          </div>\n          <div class=\"col-sm-3\"></div>\n          <div class=\"col-sm-3\"></div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <!-- Accordion card -->\n  <div class=\"card\">\n\n    <!-- Card header -->\n    <div class=\"card-header\" role=\"tab\" id=\"headingOne1\">\n      <a data-toggle=\"collapse\" class=\"collapsed\" data-parent=\"#accordionEx\" href=\"#collapseOne1\" aria-expanded=\"false\"\n        aria-controls=\"collapseOne1\">\n        <h5 class=\"mb-0\">\n          Modify user profile\n        </h5>\n      </a>\n    </div>\n\n    <!-- Card body -->\n    <div id=\"collapseOne1\" class=\"collapse show\" role=\"tabpanel\" aria-labelledby=\"headingOne1\"\n      data-parent=\"#accordionEx\">\n      <div class=\"card-body\" [hidden]=\"submitted\">\n\n        <form name=\"form\" class=\"\" (ngSubmit)=\"f.form.valid && onSubmit()\" #f=\"ngForm\" novalidate>\n          <div class=\"container-fluid\">\n            <div class=\"row\">\n\n              <div class=\"col-sm-4\">\n                <div class=\"form-group\">\n                  <label for=\"phoneNumber\">Phone number</label>\n                  <input type=\"text\" class=\"form-control pb_height-50 reverse\" name=\"phoneNumber\"\n                    value=\"model.phoneNumber\" [(ngModel)]=\"model.phoneNumber\" #phoneNumber=\"ngModel\"\n                    [ngClass]=\"{ 'is-invalid': f.submitted && phoneNumber.invalid }\" pattern=\"^\\d*$\" required />\n                  <div *ngIf=\"f.submitted && phoneNumber.invalid\" class=\"invalid-feedback\">\n                    <div *ngIf=\"phoneNumber.errors.required\">Phone number is required</div>\n                    <div *ngIf=\"phoneNumber.errors.pattern\">Only numbers are allowed</div>\n                  </div>\n                </div>\n\n                <div class=\"form-group\" *ngIf=\"model.passenger\">\n                  <label for=\"creditCardNumber\">Credit card number</label>\n                  <input type=\"text\" class=\"form-control pb_height-50 reverse\" name=\"creditCardNumber\"\n                    value=\"model.passenger.creditCardNumber\" [(ngModel)]=\"model.passenger.creditCardNumber\"\n                    #creditCardNumber=\"ngModel\" pattern=\"^\\d{16}$\"\n                    [ngClass]=\"{ 'is-invalid': f.submitted && creditCardNumber.invalid }\" required />\n                  <div *ngIf=\"f.submitted && creditCardNumber.invalid\" class=\"invalid-feedback\">\n                    <div *ngIf=\"creditCardNumber.errors.required\">Credit card number is required</div>\n                    <div *ngIf=\"creditCardNumber.errors.pattern\">Credit card number should consist from 16\n                      numbers</div>\n                  </div>\n                </div>\n\n                <div class=\"form-group\" *ngIf=\"model.driver\">\n                  <label for=\"carPlateNumber\">Car plate number</label>\n                  <input type=\"text\" class=\"form-control pb_height-50 reverse\" name=\"carPlateNumber\"\n                    [(ngModel)]=\"model.driver.carPlateNumber\" #carPlateNumber=\"ngModel\"\n                    value=\"model.driver.carPlateNumber\"\n                    [ngClass]=\"{ 'is-invalid': f.submitted && carPlateNumber.invalid }\" pattern=\"^.{6}$\" required />\n                  <div *ngIf=\"f.submitted && carPlateNumber.invalid\" class=\"invalid-feedback\">\n                    <div *ngIf=\"carPlateNumber.errors.required\">Car plate number is required</div>\n                    <div *ngIf=\"carPlateNumber.errors.pattern\">Car plate number should contain 6 numbers or\n                      letters\n                    </div>\n                  </div>\n                </div>\n\n                <div class=\"form-group\" *ngIf=\"model.driver\">\n                  <label for=\"carModel\">Car model</label>\n                  <input type=\"text\" class=\"form-control pb_height-50 reverse\" name=\"carModel\"\n                    [(ngModel)]=\"model.driver.carModel\" value=\"model.driver.carModel\" #carModel=\"ngModel\"\n                    [ngClass]=\"{ 'is-invalid': f.submitted && carModel.invalid }\" required />\n                  <div *ngIf=\"f.submitted && carModel.invalid\" class=\"invalid-feedback\">\n                    <div *ngIf=\"carModel.errors.required\">Car model needs to be specified</div>\n                  </div>\n                </div>\n\n                <div class=\"form-group\" *ngIf=\"model.passenger\">\n                  <label for=\"country\">Country</label>\n                  <input type=\"text\" class=\"form-control pb_height-50 reverse\" name=\"country\"\n                    [(ngModel)]=\"model.passenger.country\" value=\"model.passenger.country\" #country=\"ngModel\"\n                    placeholder=\"Estonia\" [ngClass]=\"{ 'is-invalid': f.submitted && country.invalid }\" required />\n                  <div *ngIf=\"f.submitted && country.invalid\" class=\"invalid-feedback\">\n                    <div *ngIf=\"country.errors.required\">Country information is is required</div>\n                  </div>\n                </div>\n\n              </div>\n              <div class=\"col-sm-4\">\n                <div class=\"form-group\">\n                  <label for=\"email\">Email</label>\n                  <input type=\"text\" class=\"form-control pb_height-50 reverse\" name=\"email\" [(ngModel)]=\"model.email\"\n                    value=\"model.email\" #email=\"ngModel\" [ngClass]=\"{ 'is-invalid': f.submitted && email.invalid }\"\n                    required email />\n                  <div *ngIf=\"f.submitted && email.invalid\" class=\"invalid-feedback\">\n                    <div *ngIf=\"email.errors.required\">Email is required</div>\n                    <div *ngIf=\"email.errors.email\">Email must be a valid email address</div>\n                  </div>\n                </div>\n\n                <div class=\"form-group\" *ngIf=\"model.driver\">\n                  <label for=\"licenseNumber\">Driving License document number</label>\n                  <input type=\"text\" class=\"form-control pb_height-50 reverse\" name=\"licenseNumber\"\n                    [(ngModel)]=\"model.driver.licenseNumber\" #licenseNumber=\"ngModel\" value=\"model.driver.licenseNumber\"\n                    [ngClass]=\"{ 'is-invalid': f.submitted && licenseNumber.invalid }\" required />\n                  <div *ngIf=\"f.submitted && licenseNumber.invalid\" class=\"invalid-feedback\">\n                    <div *ngIf=\"licenseNumber.errors.required\">Driving license document number is required</div>\n                  </div>\n                </div>\n\n                <div class=\"form-group\" *ngIf=\"model.passenger\">\n                  <label for=\"ccv\">CCV</label>\n                  <input type=\"text\" class=\"form-control pb_height-50 reverse\" name=\"ccv\"\n                    [(ngModel)]=\"model.passenger.ccv\" value=\"model.passenger.ccv\" #ccv=\"ngModel\"\n                    [ngClass]=\"{ 'is-invalid': f.submitted && ccv.invalid }\" pattern=\"^\\d{3}$\" required />\n                  <div *ngIf=\"f.submitted && ccv.invalid\" class=\"invalid-feedback\">\n                    <div *ngIf=\"ccv.errors.required\">CCV is required</div>\n                    <div *ngIf=\"ccv.errors.pattern\">CCV should consist from 3 numbers</div>\n                  </div>\n                </div>\n\n                <div class=\"form-group\">\n                  <label for=\"password\">Password</label>\n                  <input type=\"password\" class=\"form-control pb_height-50 reverse\" name=\"password\"\n                    [(ngModel)]=\"model.password\" #password=\"ngModel\"\n                    [ngClass]=\"{ 'is-invalid': f.submitted && password.invalid }\" required minlength=\"6\" />\n                  <div *ngIf=\"f.submitted && password.invalid\" class=\"invalid-feedback\">\n                    <div *ngIf=\"password.errors.required\">Password is required</div>\n                    <div *ngIf=\"password.errors.minlength\">Password must be at least 6 characters</div>\n                  </div>\n                </div>\n              </div>\n              <div class=\"col-sm-4\">\n                <div class=\"form-group\" *ngIf=\"model.driver\">\n                  <label for=\"carColor\">Car color</label>\n                  <input type=\"text\" class=\"form-control pb_height-50 reverse\" name=\"carColor\"\n                    [(ngModel)]=\"model.driver.carColor\" value=\"model.driver.carColor\" #carColor=\"ngModel\"\n                    [ngClass]=\"{ 'is-invalid': f.submitted && carColor.invalid }\" required />\n                  <div *ngIf=\"f.submitted && carColor.invalid\" class=\"invalid-feedback\">\n                    <div *ngIf=\"carColor.errors.required\">Car color needs to be specified</div>\n                  </div>\n                </div>\n                <div class=\"form-group\" *ngIf=\"model.driver\">\n                  <label for=\"amountOfSeats\">Amount of sits</label>\n                  <input type=\"text\" class=\"form-control pb_height-50 reverse\" name=\"amountOfSeats\"\n                    [(ngModel)]=\"model.driver.amountOfSeats\" #amountOfSeats=\"ngModel\" value=\"model.driver.amountOfSeats\"\n                    [ngClass]=\"{ 'is-invalid': f.submitted && amountOfSeats.invalid }\" required pattern=\"^\\d{1}\" />\n                  <div *ngIf=\"f.submitted && amountOfSeats.invalid\" class=\"invalid-feedback\">\n                    <div *ngIf=\"amountOfSeats.errors.required\">Amount of sits needs to be specified</div>\n                    <div *ngIf=\"amountOfSeats.errors.pattern\">Amount of sits should be a number from 1 to 9\n                    </div>\n                  </div>\n                </div>\n                <div class=\"form-group\" *ngIf=\"model.passenger\">\n                  <label for=\"location\">City</label>\n                  <input type=\"text\" class=\"form-control pb_height-50 reverse\" name=\"location\"\n                    [(ngModel)]=\"model.passenger.location\" value=\"model.passenger.location\" #location=\"ngModel\"\n                    placeholder=\"Tallinn\" [ngClass]=\"{ 'is-invalid': f.submitted && location.invalid }\" required />\n                  <div *ngIf=\"f.submitted && location.invalid\" class=\"invalid-feedback\">\n                    <div *ngIf=\"location.errors.required\">Location information is required</div>\n                  </div>\n                </div>\n                <div class=\"form-group date-pick\" *ngIf=\"model.driver\">\n                  <mat-form-field class=\"example-full-width\">\n                    <input matInput [min]=\"minManDate\" [max]=\"maxManDate\" [matDatepicker]=\"manDate\"\n                      placeholder=\"Manufacturing year\" name=\"carManufacturingYear\"\n                      [(ngModel)]=\"model.driver.carManufacturingYear\" #carManufacturingYear=\"ngModel\" required\n                      value=\"model.driver.carManufacturingYear\" />\n                    <mat-datepicker-toggle matSuffix [for]=\"manDate\"></mat-datepicker-toggle>\n                    <mat-datepicker #manDate></mat-datepicker>\n                  </mat-form-field>\n                </div>\n                <div class=\"form-group date-pick\" *ngIf=\"model.passenger\">\n                  <mat-form-field class=\"example-full-width\">\n                    <input matInput [min]=\"minExpirationDate\" [max]=\"maxExpirationDate\" [matDatepicker]=\"expiration\"\n                      placeholder=\"Credit card expiration date\" name=\"expirationDate\"\n                      [(ngModel)]=\"model.passenger.expirationDate\" value=\"model.passenger.expirationDate\"\n                      #expirationDate=\"ngModel\" required>\n                    <mat-datepicker-toggle matSuffix [for]=\"expiration\"></mat-datepicker-toggle>\n                    <mat-datepicker #expiration></mat-datepicker>\n                  </mat-form-field>\n                </div>\n              </div>\n            </div>\n          </div>\n          <div *ngIf=\"error\" class=\"server-error form-group\">{{error}}</div>\n          <div class=\"form-group\">\n            <button class=\"btn btn-primary btn-lg btn-block pb_btn-pill  btn-shadow-blue\">Save</button>\n          </div>\n        </form>\n      </div>\n\n      <div class=\"container success-cont\" [hidden]=\"!submitted\">\n        <div class=\"row align-items-center justify-content-center\">\n          <div class=\"col-md-3\"></div>\n          <div class=\"col-md-6 relative align-self-center\">\n            <img src=\"assets/images/green.png\" class=\"success-img\">\n            <h1 class=\"mb-4 mt-0 text-center\">User profile has been updated!</h1>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"card\">\n      <div class=\"card-header\" role=\"tab\" id=\"headingTwo2\">\n        <a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#accordionEx\" href=\"#collapseTwo2\"\n          aria-expanded=\"false\" aria-controls=\"collapseTwo2\">\n          <h5 class=\"mb-0\">\n            Report lost item\n          </h5>\n        </a>\n      </div>\n      <div id=\"collapseTwo2\" class=\"collapse\" role=\"tabpanel\" aria-labelledby=\"headingTwo2\" data-parent=\"#accordionEx\">\n        <div class=\"card-body\">\n          <app-lostitem></app-lostitem>\n        </div>\n      </div>\n    </div>\n    <div class=\"card\">\n      <!-- Card header -->\n      <div class=\"card-header\" role=\"tab\" id=\"headingThree3\">\n        <a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#accordionEx\" href=\"#collapseThree3\"\n          aria-expanded=\"false\" aria-controls=\"collapseThree3\">\n          <h5 class=\"mb-0\">\n            History\n          </h5>\n        </a>\n      </div>\n      <!-- Card body -->\n      <div id=\"collapseThree3\" class=\"collapse\" role=\"tabpanel\" aria-labelledby=\"headingThree3\"\n        data-parent=\"#accordionEx\">\n        <div class=\"card-body\">\n          <h1 mat-dialog-title> Your orders history: </h1>\n          <mat-action-list>\n            <a mat-list-item (click)=\"openOrderPopup(order)\" *ngFor=\"let order of orders\">\n              {{order.date.substring(0,10)}}: From {{order.fromName}} to {{order.whereName}}</a>\n          </mat-action-list>\n        </div>\n      </div>\n    </div>\n  </div>\n"

/***/ }),

/***/ "./src/app/profile/profile.component.ts":
/*!**********************************************!*\
  !*** ./src/app/profile/profile.component.ts ***!
  \**********************************************/
/*! exports provided: ProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileComponent", function() { return ProfileComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _models_user__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../models/user */ "./src/app/models/user.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ngx_logger__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-logger */ "./node_modules/ngx-logger/esm5/ngx-logger.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_order_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../services/order.service */ "./src/app/services/order.service.ts");
/* harmony import */ var _services_message_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../services/message.service */ "./src/app/services/message.service.ts");










var ProfileComponent = /** @class */ (function () {
    function ProfileComponent(userService, messageService, orderService, logger, http, router) {
        var _this = this;
        this.userService = userService;
        this.messageService = messageService;
        this.orderService = orderService;
        this.logger = logger;
        this.http = http;
        this.router = router;
        this.submitted = false;
        this.model = new _models_user__WEBPACK_IMPORTED_MODULE_3__["User"]();
        this.model = this.userService.currentUser;
        userService.getProfilePicture().subscribe(function (result) {
            if (result.fileDownloadUri !== null) {
                _this.imageUrl = 'http://localhost:8080' + result.fileDownloadUri;
            }
        }, function (error) {
            _this.logger.error("ProfileComponent: Couldnt get user picture: " + JSON.stringify(error));
            _this.router.navigate(['']);
        });
        orderService.getOrdersHistory().subscribe(function (result) {
            if (result._embedded) {
                _this.orders = result._embedded.orderDtoList;
            }
        }, function (error) {
            _this.logger.error("ProfileComponent: Couldnt get orders history: " + JSON.stringify(error));
            _this.router.navigate(['']);
        });
    }
    ProfileComponent.prototype.ngOnInit = function () { };
    ProfileComponent.prototype.onSubmit = function () {
        this.save();
        this.logger.debug("ProfileComponent: Updated the user: " + JSON.stringify(this.model));
    };
    ProfileComponent.prototype.openOrderPopup = function (order) {
        this.messageService.showSummaryPopup(order);
    };
    ProfileComponent.prototype.onFileChanged = function (event) {
        this.selectedFile = event.target.files[0];
    };
    ProfileComponent.prototype.onUpload = function () {
        var _this = this;
        var uploadData = new FormData();
        uploadData.append('file', this.selectedFile, this.selectedFile.name);
        this.http.post('/user/uploadDriverPhoto', uploadData).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])((function (data) { return data; })))
            .subscribe(function (result) {
            _this.imageUrl = 'http://localhost:8080' + result.fileDownloadUri;
        }, function (error) {
            _this.logger.error("ProfileComponent: Couldnt upload user picture: " + JSON.stringify(error));
            _this.router.navigate(['']);
        });
    };
    ProfileComponent.prototype.getProfilePicture = function (id) {
        return this.http.get(id);
    };
    ProfileComponent.prototype.save = function () {
        var _this = this;
        this.userService.updateUser(this.model)
            .subscribe(function (data) {
            _this.logger.debug("ProfileComponent: updated the user response from backend: " + JSON.stringify(data));
            _this.submitted = true;
            setTimeout(function () {
                _this.submitted = false;
            }, 3000); //5s
        }, function (error) {
            _this.submitted = false;
            _this.error = error.error.message;
            _this.logger.error("ProfileComponent: " + _this.error);
            _this.router.navigate(['']);
        });
    };
    ProfileComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-profile',
            template: __webpack_require__(/*! ./profile.component.html */ "./src/app/profile/profile.component.html"),
            styles: [__webpack_require__(/*! ./profile.component.css */ "./src/app/profile/profile.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"], _services_message_service__WEBPACK_IMPORTED_MODULE_9__["MessageService"], _services_order_service__WEBPACK_IMPORTED_MODULE_8__["OrderService"], ngx_logger__WEBPACK_IMPORTED_MODULE_5__["NGXLogger"], _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], ProfileComponent);
    return ProfileComponent;
}());



/***/ }),

/***/ "./src/app/register-user/register-user.component.css":
/*!***********************************************************!*\
  !*** ./src/app/register-user/register-user.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn-group.special {\r\n  display: flex;\r\n}\r\n\r\n.special .btn {\r\n  flex: 1\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVnaXN0ZXItdXNlci9yZWdpc3Rlci11c2VyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxhQUFhO0FBQ2Y7O0FBRUE7RUFDRTtBQUNGIiwiZmlsZSI6InNyYy9hcHAvcmVnaXN0ZXItdXNlci9yZWdpc3Rlci11c2VyLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYnRuLWdyb3VwLnNwZWNpYWwge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbn1cclxuXHJcbi5zcGVjaWFsIC5idG4ge1xyXG4gIGZsZXg6IDFcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/register-user/register-user.component.html":
/*!************************************************************!*\
  !*** ./src/app/register-user/register-user.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section style=\"margin:25px;\">\n  <div class=\"container\" [hidden]=\"submitted\">\n    <div class=\"row align-items-center justify-content-center\">\n      <div class=\"col-md-3\"></div>\n      <div class=\"col-md-6 relative align-self-center\">\n\n        <form name=\"form\" class=\"bg-white rounded pb_form_v1\" (ngSubmit)=\"f.form.valid && onSubmitRider()\" #f=\"ngForm\"\n          novalidate>\n\n          <div class=\"btn-group btn-group-toggle btn-group special\" data-toggle=\"buttons\">\n            <button class=\"btn btn-primary\" type=\"radio\" name=\"options\" id=\"option1\" autocomplete=\"off\" checked\n              (click)=\"changeToPassengerForm()\">Register as\n              passenger\n            </button>\n            <button class=\"btn btn-secondary\" type=\"radio\" name=\"options\" id=\"option2\" autocomplete=\"off\"\n              (click)=\"changeToDriverForm()\">Become a driver\n            </button>\n          </div>\n          <br>\n          <p *ngIf=\"typePassenger\" class=\"pb_font-14\">Manage your payment options, review trip history, and more.</p>\n          <p *ngIf=\"!typePassenger\" class=\"pb_font-14\">Earn extra money driving</p>\n\n          <br>\n\n\n          <div class=\"form-group\">\n            <label for=\"firstname\">First Name</label>\n            <input type=\"text\" class=\"form-control pb_height-50 reverse\" name=\"firstname\" [(ngModel)]=\"model.firstname\"\n              #firstname=\"ngModel\" placeholder=\"John\" pattern=\"^\\D{4,}$\"\n              [ngClass]=\"{ 'is-invalid': f.submitted && firstname.invalid }\" required />\n            <div *ngIf=\"f.submitted && firstname.invalid\" class=\"invalid-feedback\">\n              <div *ngIf=\"firstname.errors.required\">First Name is required</div>\n              <div *ngIf=\"firstname.errors.pattern\">First name should contain at least 4 letters without numbers\n              </div>\n\n\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <label for=\"lastname\">Last Name</label>\n            <input type=\"text\" class=\"form-control pb_height-50 reverse\" name=\"lastname\" [(ngModel)]=\"model.lastname\"\n              #lastname=\"ngModel\" placeholder=\"Smith\" [ngClass]=\"{ 'is-invalid': f.submitted && lastname.invalid }\"\n              pattern=\"^\\D{4,}$\" required />\n            <div *ngIf=\"f.submitted && lastname.invalid\" class=\"invalid-feedback\">\n              <div *ngIf=\"lastname.errors.required\">Last Name is required</div>\n              <div *ngIf=\"lastname.errors.pattern\">Last name should be at least 4 letters and no numbers</div>\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <label for=\"username\">User Name</label>\n            <input type=\"text\" class=\"form-control pb_height-50 reverse\" name=\"username\" [(ngModel)]=\"model.username\"\n              #username=\"ngModel\" placeholder=\"jsmith\" pattern=\"^\\D{4,}$\"\n              [ngClass]=\"{ 'is-invalid': f.submitted && username.invalid }\" required />\n            <div *ngIf=\"f.submitted && username.invalid\" class=\"invalid-feedback\">\n              <div *ngIf=\"username.errors.required\">User name is required</div>\n              <div *ngIf=\"username.errors.pattern\">User name should contain at least 4 letters without numbers</div>\n            </div>\n          </div>\n\n          <div class=\"form-group\">\n            <label for=\"email\">Email</label>\n            <input type=\"text\" class=\"form-control pb_height-50 reverse\" name=\"email\" [(ngModel)]=\"model.email\"\n              #email=\"ngModel\" placeholder=\"johns@gmail.com\" [ngClass]=\"{ 'is-invalid': f.submitted && email.invalid }\"\n              required email />\n            <div *ngIf=\"f.submitted && email.invalid\" class=\"invalid-feedback\">\n              <div *ngIf=\"email.errors.required\">Email is required</div>\n              <div *ngIf=\"email.errors.email\">Email must be a valid email address</div>\n            </div>\n          </div>\n\n\n          <div *ngIf=\"typePassenger\">\n\n            <div class=\"form-group\">\n              <label for=\"phoneNumber\">Phone number</label>\n              <input type=\"text\" class=\"form-control pb_height-50 reverse\" name=\"phoneNumber\"\n                [(ngModel)]=\"model.phoneNumber\" #phoneNumber=\"ngModel\" placeholder=\"55123456\"\n                [ngClass]=\"{ 'is-invalid': f.submitted && phoneNumber.invalid }\" pattern=\"^\\d{6,}$\" required />\n              <div *ngIf=\"f.submitted && phoneNumber.invalid\" class=\"invalid-feedback\">\n                <div *ngIf=\"phoneNumber.errors.required\">Phone number is required</div>\n                <div *ngIf=\"phoneNumber.errors.pattern\">Phone number is incorrect</div>\n              </div>\n            </div>\n\n\n            <div class=\"form-group\">\n              <mat-form-field class=\"example-full-width\">\n                <input matInput [min]=\"minBirthdayDate\" [max]=\"maxBirthdayDate\" [matDatepicker]=\"birthpick\"\n                  placeholder=\"Birthday\" name=\"birthDate\" [(ngModel)]=\"model.birthDate\" #expirationDate=\"ngModel\"\n                  required>\n                <mat-datepicker-toggle matSuffix [for]=\"birthpick\"></mat-datepicker-toggle>\n                <mat-datepicker #birthpick></mat-datepicker>\n              </mat-form-field>\n            </div>\n\n            <div class=\"form-group\">\n              <label for=\"country\">Country</label>\n              <input type=\"text\" class=\"form-control pb_height-50 reverse\" name=\"country\" [(ngModel)]=\"model.country\"\n                #country=\"ngModel\" placeholder=\"Estonia\" [ngClass]=\"{ 'is-invalid': f.submitted && country.invalid }\"\n                pattern=\"^\\D{4,}$\" required />\n              <div *ngIf=\"f.submitted && country.invalid\" class=\"invalid-feedback\">\n                <div *ngIf=\"country.errors.required\">Country information is is required</div>\n                <div *ngIf=\"country.errors.pattern\">Country should have at least 4 letters</div>\n              </div>\n            </div>\n\n            <div class=\"form-group\">\n              <label for=\"creditCardNumber\">Credit card number</label>\n              <input type=\"text\" class=\"form-control pb_height-50 reverse\" name=\"creditCardNumber\"\n                [(ngModel)]=\"model.creditCardNumber\" #creditCardNumber=\"ngModel\" placeholder=\"1234 1234 1234 1234\"\n                pattern=\"^\\d{16}$\" [ngClass]=\"{ 'is-invalid': f.submitted && creditCardNumber.invalid }\" required />\n              <div *ngIf=\"f.submitted && creditCardNumber.invalid\" class=\"invalid-feedback\">\n                <div *ngIf=\"creditCardNumber.errors.required\">Credit card number is required</div>\n                <div *ngIf=\"creditCardNumber.errors.pattern\">Credit card number should consist from 16 numbers</div>\n              </div>\n            </div>\n\n            <div class=\"form-group\">\n              <label for=\"ccv\">CCV</label>\n              <input type=\"text\" class=\"form-control pb_height-50 reverse\" name=\"ccv\" [(ngModel)]=\"model.ccv\"\n                #ccv=\"ngModel\" value=\"124\" [ngClass]=\"{ 'is-invalid': f.submitted && ccv.invalid }\" placeholder=\"123\"\n                pattern=\"^\\d{3}$\" required />\n              <div *ngIf=\"f.submitted && ccv.invalid\" class=\"invalid-feedback\">\n                <div *ngIf=\"ccv.errors.required\">CCV is required</div>\n                <div *ngIf=\"ccv.errors.pattern\">CCV should consist from 3 numbers</div>\n\n              </div>\n            </div>\n\n            <div class=\"form-group\">\n              <mat-form-field class=\"example-full-width\">\n                <input matInput [min]=\"minExpirationDate\" [max]=\"maxExpirationDate\" [matDatepicker]=\"expiration\"\n                  placeholder=\"Credit card expiration date\" name=\"expirationDate\" [(ngModel)]=\"model.expirationDate\"\n                  #expirationDate=\"ngModel\" required>\n                <mat-datepicker-toggle matSuffix [for]=\"expiration\"></mat-datepicker-toggle>\n                <mat-datepicker #expiration></mat-datepicker>\n              </mat-form-field>\n            </div>\n\n\n            <div class=\"form-group\">\n              <label for=\"location\">City</label>\n              <input type=\"text\" class=\"form-control pb_height-50 reverse\" name=\"location\" [(ngModel)]=\"model.location\"\n                #location=\"ngModel\" placeholder=\"Tallinn\" [ngClass]=\"{ 'is-invalid': f.submitted && location.invalid }\"\n                pattern=\"^\\D{4,}$\" required />\n              <div *ngIf=\"f.submitted && location.invalid\" class=\"invalid-feedback\">\n                <div *ngIf=\"location.errors.required\">Location information is required</div>\n                <div *ngIf=\"location.errors.pattern\">City should have at least 4 letters</div>\n\n              </div>\n            </div>\n\n          </div>\n\n\n\n\n\n          <div *ngIf=\"!typePassenger\">\n            <div class=\"form-group\">\n              <label for=\"phoneNumber\">Phone number</label>\n              <input type=\"text\" class=\"form-control pb_height-50 reverse\" name=\"phoneNumber\"\n                [(ngModel)]=\"model.phoneNumber\" #phoneNumber=\"ngModel\" placeholder=\"55123456\"\n                [ngClass]=\"{ 'is-invalid': f.submitted && phoneNumber.invalid }\" pattern=\"^\\d*$\" required />\n              <div *ngIf=\"f.submitted && phoneNumber.invalid\" class=\"invalid-feedback\">\n                <div *ngIf=\"phoneNumber.errors.required\">Phone number is required</div>\n                <div *ngIf=\"phoneNumber.errors.pattern\">Only numbers are allowed</div>\n              </div>\n            </div>\n\n            <div class=\"form-group\">\n              <label for=\"licenseNumber\">Driving License document number</label>\n              <input type=\"text\" class=\"form-control pb_height-50 reverse\" name=\"licenseNumber\"\n                [(ngModel)]=\"model.licenseNumber\" #licenseNumber=\"ngModel\"\n                [ngClass]=\"{ 'is-invalid': f.submitted && licenseNumber.invalid }\" required />\n              <div *ngIf=\"f.submitted && licenseNumber.invalid\" class=\"invalid-feedback\">\n                <div *ngIf=\"licenseNumber.errors.required\">Driving license document number is required</div>\n              </div>\n            </div>\n\n            <div class=\"form-group\">\n              <label for=\"carPlateNumber\">Car plate number</label>\n              <input type=\"text\" class=\"form-control pb_height-50 reverse\" name=\"carPlateNumber\"\n                [(ngModel)]=\"model.carPlateNumber\" #carPlateNumber=\"ngModel\"\n                [ngClass]=\"{ 'is-invalid': f.submitted && carPlateNumber.invalid }\" pattern=\"^.{6}$\" required />\n              <div *ngIf=\"f.submitted && carPlateNumber.invalid\" class=\"invalid-feedback\">\n                <div *ngIf=\"carPlateNumber.errors.required\">Car plate number is required</div>\n                <div *ngIf=\"carPlateNumber.errors.pattern\">Car plate number should contain 6 numbers or letters\n                </div>\n\n              </div>\n            </div>\n\n            <div class=\"form-group\">\n              <mat-form-field class=\"example-full-width\">\n                <input matInput [min]=\"minManDate\" [max]=\"maxManDate\" [matDatepicker]=\"manDate\"\n                  placeholder=\"Manufacturing year\" name=\"carManufacturingYear\" [(ngModel)]=\"model.carManufacturingYear\"\n                  #carManufacturingYear=\"ngModel\" required>\n                <mat-datepicker-toggle matSuffix [for]=\"manDate\"></mat-datepicker-toggle>\n                <mat-datepicker #manDate></mat-datepicker>\n              </mat-form-field>\n            </div>\n\n            <div class=\"form-group\">\n              <label for=\"amountOfSeats\">Amount of sits</label>\n              <input type=\"text\" class=\"form-control pb_height-50 reverse\" name=\"amountOfSeats\"\n                [(ngModel)]=\"model.amountOfSeats\" #amountOfSeats=\"ngModel\"\n                [ngClass]=\"{ 'is-invalid': f.submitted && amountOfSeats.invalid }\" required pattern=\"^\\d{1}\" />\n              <div *ngIf=\"f.submitted && amountOfSeats.invalid\" class=\"invalid-feedback\">\n                <div *ngIf=\"amountOfSeats.errors.required\">Amount of sits needs to be specified</div>\n                <div *ngIf=\"amountOfSeats.errors.pattern\">Amount of sits should be a number from 1 to 9</div>\n\n              </div>\n            </div>\n\n            <div class=\"form-group\">\n              <label for=\"carModel\">Car model</label>\n              <input type=\"text\" class=\"form-control pb_height-50 reverse\" name=\"carModel\" [(ngModel)]=\"model.carModel\"\n                #carModel=\"ngModel\" [ngClass]=\"{ 'is-invalid': f.submitted && carModel.invalid }\" required />\n              <div *ngIf=\"f.submitted && carModel.invalid\" class=\"invalid-feedback\">\n                <div *ngIf=\"carModel.errors.required\">Car model needs to be specified</div>\n              </div>\n            </div>\n\n            <div class=\"form-group\">\n              <label for=\"carColor\">Car color</label>\n              <input type=\"text\" class=\"form-control pb_height-50 reverse\" name=\"carColor\" [(ngModel)]=\"model.carColor\"\n                #carColor=\"ngModel\" [ngClass]=\"{ 'is-invalid': f.submitted && carColor.invalid }\" required />\n              <div *ngIf=\"f.submitted && carColor.invalid\" class=\"invalid-feedback\">\n                <div *ngIf=\"carColor.errors.required\">Car color needs to be specified</div>\n              </div>\n            </div>\n          </div>\n\n          <div class=\"form-group\">\n            <label for=\"password\">Password</label>\n            <input type=\"password\" class=\"form-control pb_height-50 reverse\" name=\"password\"\n              [(ngModel)]=\"model.password\" #password=\"ngModel\"\n              [ngClass]=\"{ 'is-invalid': f.submitted && password.invalid }\" required minlength=\"6\" />\n            <div *ngIf=\"f.submitted && password.invalid\" class=\"invalid-feedback\">\n              <div *ngIf=\"password.errors.required\">Password is required</div>\n              <div *ngIf=\"password.errors.minlength\">Password must be at least 6 characters</div>\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <input type=\"checkbox\" name=\"check\" [(ngModel)]=\"model.check\" #check=\"ngModel\"\n              [ngClass]=\"{ 'is-invalid': f.submitted && check.invalid }\" required checked> I have read and agree to\n            the\n            terms of service\n            <div *ngIf=\"f.submitted && check.invalid\" class=\"invalid-feedback\">\n              <div *ngIf=\"check.errors.checked\">You must agree with terms of service</div>\n            </div>\n          </div>\n          <div *ngIf=\"error\" class=\"server-error form-group\">{{error}}</div>\n          <div class=\"form-group\">\n            <button class=\"btn btn-primary btn-lg btn-block pb_btn-pill  btn-shadow-blue\">Register</button>\n          </div>\n        </form>\n      </div>\n      <div class=\"col-md-3\"></div>\n    </div>\n  </div>\n\n  <div class=\"container success-cont\" [hidden]=\"!submitted\">\n    <div class=\"row align-items-center justify-content-center\">\n      <div class=\"col-md-3\"></div>\n      <div class=\"col-md-6 relative align-self-center\">\n        <img src=\"assets/images/green.png\" class=\"success-img\" />\n        <h1 class=\"mb-4 mt-0 text-center\">You have successfully registred!</h1>\n      </div>\n      <div class=\"col-md-3\"></div>\n    </div>\n  </div>\n\n</section>\n"

/***/ }),

/***/ "./src/app/register-user/register-user.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/register-user/register-user.component.ts ***!
  \**********************************************************/
/*! exports provided: RegisterUserComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterUserComponent", function() { return RegisterUserComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ngx_logger__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-logger */ "./node_modules/ngx-logger/esm5/ngx-logger.js");





var RegisterUserComponent = /** @class */ (function () {
    function RegisterUserComponent(userService, router, logger) {
        this.userService = userService;
        this.router = router;
        this.logger = logger;
        this.submitted = false;
        this.model = {};
        this.minExpirationDate = new Date();
        this.maxExpirationDate = new Date(2030, 0, 1);
        this.minBirthdayDate = new Date(1920, 0, 1);
        this.maxBirthdayDate = new Date();
        this.registrationConfirmation = false;
        this.typePassenger = true;
    }
    RegisterUserComponent.prototype.changeToPassengerForm = function () {
        this.typePassenger = true;
    };
    RegisterUserComponent.prototype.changeToDriverForm = function () {
        this.typePassenger = false;
    };
    RegisterUserComponent.prototype.onSubmitRider = function () {
        var role;
        var driver;
        var passenger;
        if (this.typePassenger) {
            role = "ROLE_PASSENGER";
            driver = null;
            passenger = {
                birthdate: this.model.birthDate,
                location: this.model.location,
                country: this.model.country,
                creditCardNumber: this.model.creditCardNumber,
                expirationDate: this.model.expirationDate,
                active: true,
                ccv: this.model.ccv
            };
        }
        else {
            role = "ROLE_DRIVER";
            passenger = null;
            driver = {
                licenseNumber: this.model.licenseNumber,
                driversPhoto: null,
                carPlateNumber: this.model.carPlateNumber,
                carManufacturingYear: this.model.carManufacturingYear,
                carModel: this.model.carModel,
                amountOfSeats: this.model.amountOfSeats,
                carColor: this.model.carColor,
                active: true
            };
        }
        this.typePassenger ? role = "ROLE_PASSENGER" : role = "ROLE_DRIVER";
        this.user = {
            firstname: this.model.firstname,
            lastname: this.model.lastname,
            username: this.model.username,
            password: this.model.password,
            email: this.model.email,
            phoneNumber: this.model.phoneNumber,
            enabled: true,
            driver: driver,
            passenger: passenger,
            authorities: [{ authority: role }]
        };
        this.save();
        this.logger.debug("RegisterUserComponent: Saving the user: " + JSON.stringify(this.user));
    };
    RegisterUserComponent.prototype.save = function () {
        var _this = this;
        this.userService.registerUser(this.user)
            .subscribe(function (data) {
            _this.submitted = true;
            _this.logger.debug("RegisterUserComponent: Registered user: " + JSON.stringify(data));
            setTimeout(function () {
                _this.router.navigate(['']);
            }, 3000); //5s
        }, function (error) {
            _this.submitted = false;
            _this.error = error.error.message;
            _this.logger.error("RegisterUserComponent: " + error.error.message);
            _this.router.navigate(['']);
        });
    };
    RegisterUserComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-register-user',
            template: __webpack_require__(/*! ./register-user.component.html */ "./src/app/register-user/register-user.component.html"),
            styles: [__webpack_require__(/*! ./register-user.component.css */ "./src/app/register-user/register-user.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], ngx_logger__WEBPACK_IMPORTED_MODULE_4__["NGXLogger"]])
    ], RegisterUserComponent);
    return RegisterUserComponent;
}());



/***/ }),

/***/ "./src/app/services/location.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/location.service.ts ***!
  \**********************************************/
/*! exports provided: LocationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocationService", function() { return LocationService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/BehaviorSubject */ "./node_modules/rxjs-compat/_esm5/BehaviorSubject.js");
/* harmony import */ var _models_coordinates__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../models/coordinates */ "./src/app/models/coordinates.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var ngx_logger__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-logger */ "./node_modules/ngx-logger/esm5/ngx-logger.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");







var LocationService = /** @class */ (function () {
    function LocationService(logger, router) {
        this.logger = logger;
        this.router = router;
        this.coordinates = new _models_coordinates__WEBPACK_IMPORTED_MODULE_3__["Coordinates"]();
        this.coordinateSource = new rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](this.coordinates);
        this.currentCoords = this.coordinateSource.asObservable();
        this.navigation = new _models_coordinates__WEBPACK_IMPORTED_MODULE_3__["Coordinates"]();
        this.navigationSource = new rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](this.navigation);
        this.navigationStatus = this.navigationSource.asObservable();
    }
    LocationService.prototype.ngOnInit = function () {
        this.setCurrentPosition();
    };
    LocationService.prototype.calculateDistance = function (coordinates) {
        var distance = Math.sqrt(Math.pow(coordinates.fromLat - coordinates.whereLat, 2) + (Math.pow(coordinates.fromLong - coordinates.whereLong, 2))) * 100;
        this.logger.debug("LocationService: Calculated distance: " + distance + " based on coordinates " + JSON.stringify(coordinates));
        this.updateWhereCoordinatesWithName(coordinates);
        return distance;
    };
    LocationService.prototype.changeCoordinates = function (coords) {
        this.coordinateSource.next(coords);
        this.logger.debug("LocationService: Changed coordinates variable from " + JSON.stringify(this.coordinates) + " to " + JSON.stringify(coords));
    };
    LocationService.prototype.resetDestinationCoordinates = function () {
        this.coordinates.whereLat = null;
        this.coordinates.whereLong = null;
        this.setCurrentPosition();
        this.coordinateSource.next(this.coordinates);
        this.logger.debug("LocationService: Reset where coordinates");
    };
    LocationService.prototype.updateFromCoordinatesWithName = function (input) {
        var name = input.split(",")[0];
        var coords = this.coordinates;
        coords.fromName = name;
        this.coordinateSource.next(coords);
        this.logger.debug("LocationService: Changed coordinates name from " + JSON.stringify(this.coordinates.fromName) + " to " + JSON.stringify(name));
    };
    LocationService.prototype.updateWhereCoordinatesWithName = function (coordinates) {
        var _this = this;
        if (coordinates.whereLat && coordinates.whereLong) {
            this.geocode(coordinates.whereLat, coordinates.whereLong).subscribe(function (result) {
                _this.logger.debug("LocationService: Reverse geocoding address from coordinates: " + result[0].formatted_address);
                coordinates.whereName = result[0].formatted_address.split(",")[0];
                _this.coordinateSource.next(coordinates);
                _this.coordinates = coordinates;
                _this.logger.debug("LocationService: Changed navigation coordinates name from " + JSON.stringify(_this.navigation) + " to " + JSON.stringify(coordinates));
            }, function (error) {
                _this.logger.error("LocationService: Error while reverse geocoding address from coordinates: " + error);
                _this.router.navigate(['']);
            });
            var coords = this.coordinates;
            coords.whereName = name;
            this.coordinateSource.next(coords);
            this.logger.debug("LocationService: Changed coordinates name from " + JSON.stringify(this.coordinates.whereName) + " to " + JSON.stringify(name));
        }
    };
    LocationService.prototype.setNavigationCoordinates = function (order) {
        if (order === null || order.status === "CANCELLED" || order.status === "ACCEPTED" || order.status === "PAID" || order.status === "TERMINATED") {
            this.navigationSource.next(null);
            this.logger.debug("LocationService: Order doesn't exist or is inactive set navigation coordinates to null");
        }
        else {
            var coordinates = this.getCoordinatesFromOrder(order);
            this.navigationSource.next(coordinates);
            this.navigation = coordinates;
            this.logger.debug("LocationService: Order exists and is active, set navigation coordinates to " + JSON.stringify(this.navigation));
        }
    };
    LocationService.prototype.getCoordinatesFromOrder = function (order) {
        var coordinates = new _models_coordinates__WEBPACK_IMPORTED_MODULE_3__["Coordinates"]();
        coordinates.fromLat = order.fromLat;
        coordinates.fromLong = order.fromLong;
        coordinates.whereLat = order.whereLat;
        coordinates.whereLong = order.whereLong;
        coordinates.fromName = order.fromName;
        coordinates.whereName = order.whereName;
        this.logger.debug("LocationService: Fetched coordinates " + coordinates + " from the order " + JSON.stringify(order));
        return coordinates;
    };
    LocationService.prototype.geocode = function (lat, lng) {
        var _this = this;
        return rxjs__WEBPACK_IMPORTED_MODULE_4__["Observable"].create(function (observer) {
            // Invokes geocode method of Google Maps API geocoding.
            var latlng = new google.maps.LatLng(lat, lng);
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ location: latlng }, (function (results, status) {
                if (status === google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
                    setTimeout(function () {
                    }, 1000);
                }
                else if (status === google.maps.GeocoderStatus.OK) {
                    observer.next(results);
                    observer.complete();
                }
                else {
                    _this.logger.error('LocationService: Geocoding service: geocoder failed due to: ' + status);
                    observer.error(status);
                }
            }));
        });
    };
    LocationService.prototype.setCurrentPosition = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            navigator.geolocation.getCurrentPosition(function (resp) {
                _this.coordinates.fromLong = resp.coords.longitude;
                _this.coordinates.fromLat = resp.coords.latitude;
                _this.coordinateSource.next(_this.coordinates);
                _this.geocode(resp.coords.latitude, resp.coords.longitude).subscribe(function (result) {
                    _this.logger.debug("LocationService: Reverse geocoding address from coordinates: " + result[0].formatted_address);
                    _this.updateFromCoordinatesWithName(result[0].formatted_address);
                });
                _this.logger.debug("LocationService: Set current position to " + JSON.stringify(_this.coordinates));
                resolve({
                    lng: resp.coords.longitude,
                    lat: resp.coords.latitude
                });
            }, function (err) {
                reject(err);
                _this.logger.error("LocationService: Error getting current position");
            });
        });
    };
    LocationService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [ngx_logger__WEBPACK_IMPORTED_MODULE_5__["NGXLogger"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]])
    ], LocationService);
    return LocationService;
}());



/***/ }),

/***/ "./src/app/services/menu.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/menu.service.ts ***!
  \******************************************/
/*! exports provided: MenuService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuService", function() { return MenuService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _order_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./order.service */ "./src/app/services/order.service.ts");
/* harmony import */ var _models_link__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../models/link */ "./src/app/models/link.ts");
/* harmony import */ var rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/BehaviorSubject */ "./node_modules/rxjs-compat/_esm5/BehaviorSubject.js");
/* harmony import */ var _web_socket_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./web-socket.service */ "./src/app/services/web-socket.service.ts");
/* harmony import */ var _stomp_ng2_stompjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @stomp/ng2-stompjs */ "./node_modules/@stomp/ng2-stompjs/fesm5/stomp-ng2-stompjs.js");
/* harmony import */ var _message_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./message.service */ "./src/app/services/message.service.ts");
/* harmony import */ var _location_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./location.service */ "./src/app/services/location.service.ts");
/* harmony import */ var ngx_logger__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-logger */ "./node_modules/ngx-logger/esm5/ngx-logger.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");











var MenuService = /** @class */ (function () {
    function MenuService(orderService, webSocketService, messageService, locationService, logger, router) {
        this.orderService = orderService;
        this.webSocketService = webSocketService;
        this.messageService = messageService;
        this.locationService = locationService;
        this.logger = logger;
        this.router = router;
        this.state = "NOT CONNECTED";
        this.links = new _models_link__WEBPACK_IMPORTED_MODULE_3__["Link"]();
        this.linksSource = new rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_4__["BehaviorSubject"](this.links);
        this.linksStatus = this.linksSource.asObservable();
    }
    MenuService.prototype.monitorBroadcastMsg = function () {
        var _this = this;
        // Subscribe to its stream (to listen on messages)
        this.webSocketService.stream().subscribe(function (message) {
            if (message.body !== "\"null\"" && message.body !== "") {
                var response = JSON.parse(message.body);
                var responseOrder = response[0];
                var action_1 = response[1];
                _this.logger.debug("MenuService: Received broadcased message: " + JSON.stringify(message.body));
                if (_this.checkIfMessageRequireActions(responseOrder, action_1)) {
                    _this.orderService.getOrderById(responseOrder.orderId).subscribe(function (order) {
                        _this.updateLinks(order);
                        // this.locationService.setNavigationCoordinates(order); // is it needed?
                        if (action_1 === "terminate" || action_1 === "pay" || action_1 === "summary") {
                            _this.generateSummaryPopup(order);
                        }
                        else {
                            var type = "info";
                            if (action_1 === "accept") {
                                type = action_1;
                            }
                            _this.messageService.showInfoPopup(order, action_1, type);
                            _this.orderService.setLastOrder(order);
                        }
                    }, function (error) {
                        _this.logger.error("MenuService: Error while getting order by id: " + JSON.stringify(error));
                        _this.router.navigate(['']);
                    });
                }
            }
        });
        // Subscribe to its state (to know its connected or not)
        this.webSocketService.state().subscribe(function (state) {
            _this.state = _stomp_ng2_stompjs__WEBPACK_IMPORTED_MODULE_6__["StompState"][state];
        });
    };
    MenuService.prototype.generateSummaryPopup = function (order) {
        var _this = this;
        if (this.currentUser.authorities[0].authority === "ROLE_PASSENGER" && order.status === "PAID") {
            this.messageService.showSummaryPopupWithRating(order).subscribe(function (rating) {
                _this.orderService.addRating(rating, order).subscribe(function (response) {
                    _this.logger.debug("MenuService: Added rating " + rating + " to the order " + order.orderId);
                    _this.webSocketService.send("/server-receiver", [response, "summary"]);
                }, function (error) {
                    _this.logger.error("MenuService: Couldnt add a rating " + rating + " to the order " + order.orderId + " due to " + error);
                    _this.router.navigate(['']);
                });
            }), function (error) {
                _this.logger.error("MenuService: Error while receiving ride rating: " + JSON.stringify(error));
                _this.router.navigate(['']);
            };
        }
        else {
            this.messageService.showSummaryPopup(order);
        }
        this.locationService.resetDestinationCoordinates();
    };
    MenuService.prototype.checkIfMessageRequireActions = function (responseOrder, action) {
        var status;
        if (action === "create" || (action === "cancel" && responseOrder.driver === null)) {
            status = false;
        }
        else if (action === "summary" && this.currentUser.authorities[0].authority === 'ROLE_DRIVER' && responseOrder.rating !== null) {
            status = true;
        }
        else if (action === "summary" && this.currentUser.authorities[0].authority === 'ROLE_PASSENGER' && responseOrder.rating !== null) {
            status = false;
        }
        else {
            if (this.currentUser.authorities[0].authority === 'ROLE_PASSENGER') {
                if (this.currentUser.username === responseOrder.passenger.username) {
                    this.logger.debug("MenuService: Received order belongs to the user and require action: " + action);
                    status = this.compareOrderWithLastOneRecorded(responseOrder);
                }
            }
            else if (this.currentUser.authorities[0].authority === 'ROLE_DRIVER') {
                if (this.currentUser.username === responseOrder.driver.username) {
                    this.logger.debug("MenuService: Received order belongs to the user and require action: " + action);
                    status = this.compareOrderWithLastOneRecorded(responseOrder);
                }
            }
        }
        return status;
    };
    MenuService.prototype.compareOrderWithLastOneRecorded = function (responseOrder) {
        var status = false;
        this.logger.debug("MenuService: Comparing with the last order: " + JSON.stringify(this.lastOrder) + " with received order " + JSON.stringify(responseOrder));
        if (this.lastOrder && responseOrder) {
            if (responseOrder.orderId === this.lastOrder.orderId) {
                if (JSON.stringify(this.lastOrder) !== JSON.stringify(responseOrder)) {
                    this.logger.debug("MenuService: Received order and last orders have same id but not equal");
                    status = true;
                }
            }
        }
        return status;
    };
    MenuService.prototype.updateMenu = function () {
        var _this = this;
        this.orderService.getActiveOrder().subscribe(function (activeOrder) {
            _this.logger.debug("MenuService: Received active order: " + JSON.stringify(activeOrder));
            _this.updateLinks(activeOrder);
        }, function (error) {
            _this.logger.error("MenuService: Can't receive an active order: " + JSON.stringify(error));
            _this.router.navigate(['']);
        });
    };
    MenuService.prototype.setCurrentUser = function (user) {
        this.currentUser = user;
        this.logger.debug("MenuService: Set current user to " + JSON.stringify(user));
    };
    MenuService.prototype.updateLinks = function (order) {
        var linksFromLastOrder = new _models_link__WEBPACK_IMPORTED_MODULE_3__["Link"]();
        this.logger.debug("MenuService: Updating links from order: " + JSON.stringify(linksFromLastOrder));
        if (order === null || (!order._links.accept && !order._links.cancel && !order._links.complete && !order._links.start && !order._links.create && !order._links.payment && !order._links.terminate)) {
            this.logger.debug("MenuService: Order is null or all order links are empty");
            if (this.currentUser.authorities[0].authority === 'ROLE_DRIVER') {
                this.logger.debug("MenuService: User is a driver, setting getOpen to href");
                linksFromLastOrder.getOpen = { href: "getOpen" };
            }
            else if (this.currentUser.authorities[0].authority === 'ROLE_PASSENGER') {
                this.logger.debug("MenuService: User is a passenger, setting add to href");
                linksFromLastOrder.create = { href: "add" };
            }
        }
        else {
            linksFromLastOrder = order._links;
            this.logger.debug("MenuService: Order is not null or there are no empty links");
        }
        this.linksSource.next(linksFromLastOrder);
        this.logger.debug("MenuService: Updated linksSource with new links");
        this.orderService.setLastOrder(order);
        this.links = linksFromLastOrder; // not sure why it doesnt work without it TODO
    };
    MenuService.prototype.getActionUrl = function (action) {
        switch (action) {
            case 'cancel':
                this.url = this.links.cancel.href;
                break;
            case 'complete':
                this.url = this.links.complete.href;
                break;
            case 'payment':
                this.url = this.links.payment.href;
                break;
            case 'accept':
                this.url = this.links.accept.href;
                break;
            case 'start':
                this.url = this.links.start.href;
                break;
            case 'terminate':
                this.url = this.links.terminate.href;
                break;
        }
        var actionUrl = this.url.split('/')[4] + '/' + this.url.split('/')[5];
        this.logger.debug("MenuService: Got action url: " + actionUrl + " from action: " + action);
        return actionUrl;
    };
    MenuService.prototype.getListOfOpenOrders = function () {
        var listOfOrders = this.orderService.getOpenOrders();
        this.logger.debug("MenuService: Got list of open orders: " + JSON.stringify(listOfOrders));
        return listOfOrders;
    };
    MenuService.prototype.executeAction = function (action) {
        var _this = this;
        this.logger.debug("MenuService: Execution action: " + action);
        if (action === 'create') {
            this.orderService.add().subscribe(function (result) {
                _this.logger.debug("MenuService: Added order: " + JSON.stringify(result));
                _this.orderService.setLastOrder(result);
                _this.updateLinks(result);
                _this.webSocketService.send("/server-receiver", [result, action]);
            }, function (error) {
                _this.logger.error("Could not add order " + error);
                _this.router.navigate(['']);
            });
        }
        else {
            var url_1 = this.getActionUrl(action);
            this.orderService.execute(url_1).subscribe(function (result) {
                _this.logger.debug("MenuService: Executed url: " + url_1 + ", got order: " + JSON.stringify(result));
                if (action === 'start') {
                    _this.locationService.setNavigationCoordinates(result);
                }
                else {
                    _this.locationService.setNavigationCoordinates(null);
                }
                _this.orderService.setLastOrder(result);
                _this.webSocketService.send("/server-receiver", [result, action]);
                _this.updateLinks(result);
                if (action === 'cancel' || action === 'payment' || action === 'terminate') {
                    _this.locationService.resetDestinationCoordinates();
                    _this.generateSummaryPopup(_this.lastOrder);
                    _this.logger.debug("MenuService: Reset destination coordinates");
                }
            }, function (error) {
                _this.logger.error("MenuService: Error while action execution: " + error);
                _this.router.navigate(['']);
            });
        }
    };
    MenuService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_order_service__WEBPACK_IMPORTED_MODULE_2__["OrderService"],
            _web_socket_service__WEBPACK_IMPORTED_MODULE_5__["WebSocketService"],
            _message_service__WEBPACK_IMPORTED_MODULE_7__["MessageService"],
            _location_service__WEBPACK_IMPORTED_MODULE_8__["LocationService"],
            ngx_logger__WEBPACK_IMPORTED_MODULE_9__["NGXLogger"],
            _angular_router__WEBPACK_IMPORTED_MODULE_10__["Router"]])
    ], MenuService);
    return MenuService;
}());



/***/ }),

/***/ "./src/app/services/message.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/message.service.ts ***!
  \*********************************************/
/*! exports provided: MessageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MessageService", function() { return MessageService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _message_message_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../message/message.component */ "./src/app/message/message.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var ngx_logger__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-logger */ "./node_modules/ngx-logger/esm5/ngx-logger.js");





var MessageService = /** @class */ (function () {
    function MessageService(dialog, logger) {
        this.dialog = dialog;
        this.logger = logger;
        this.rating = ["TERRIBLE", "DISAPPOINTMENT", "OK", "GOOD", "PERFECT"];
    }
    MessageService.prototype.showAllOpenOrdersPopup = function (orders) {
        this.logger.debug("MessageService: Show notification with all open orders: " + JSON.stringify(orders._embedded.orderDtoList));
        return this.dialog.open(_message_message_component__WEBPACK_IMPORTED_MODULE_2__["MessageComponent"], {
            data: {
                type: "all orders",
                orders: orders._embedded.orderDtoList,
            }
        }).afterClosed();
    };
    MessageService.prototype.showErrorPopUp = function (error) {
        this.dialog.open(_message_message_component__WEBPACK_IMPORTED_MODULE_2__["MessageComponent"], {
            data: {
                type: "error",
                info: error
            }
        });
        this.logger.debug("MessageService: Showing error popup: " + error);
    };
    MessageService.prototype.showSummaryPopup = function (order) {
        this.dialog.open(_message_message_component__WEBPACK_IMPORTED_MODULE_2__["MessageComponent"], {
            data: {
                type: "summary",
                order: order
            }
        });
        this.logger.debug("MessageService: Showing notification with ride summary");
    };
    MessageService.prototype.showSummaryPopupWithRating = function (order) {
        this.logger.debug("MessageService: Showing summary with rating");
        return this.dialog.open(_message_message_component__WEBPACK_IMPORTED_MODULE_2__["MessageComponent"], {
            data: {
                type: "summary-passenger",
                order: order
            }
        }).afterClosed();
    };
    MessageService.prototype.showPopup = function (type, message, order) {
        this.logger.debug("MessageService: Sent notification with title: " + message[0] + " and body " + message[1]);
        return this.dialog.open(_message_message_component__WEBPACK_IMPORTED_MODULE_2__["MessageComponent"], {
            data: {
                type: type,
                title: message[0],
                info: message[1],
                order: order
            }
        }).afterClosed();
    };
    MessageService.prototype.showInfoPopup = function (order, action, type) {
        var message = this.selectPopupMessageFromStatus(action, order);
        if (message[0] && message[1]) {
            return this.showPopup(type, message, order);
        }
        else
            return null;
    };
    MessageService.prototype.selectPopupMessageFromStatus = function (action, order) {
        var title;
        var message;
        switch (action) {
            case "accept":
                title = "Your ride has been accepted by " + order.driver.firstname + "!";
                message = "You can still cancel it but there will be a penalty fee of " + (order.cost / 2).toFixed(2) + " EUR";
                break;
            case "start":
                title = "Your ride has been started!";
                message = "";
                break;
            // case "summary":
            //   title = "Rating";
            //   message = `Passenger has rated the ride as ${this.rating[order.rating]}!`;
            //   break;
            case "complete":
                title = "You have arrived!";
                message = "Ride fee is: " + (order.cost).toFixed(2) + " EUR";
                break;
            case "cancel":
                title = "Passenger has cancelled the ride";
                message = "You will get a refund of " + (order.cost).toFixed(2) + " EUR";
                break;
        }
        return [title, message];
    };
    MessageService.prototype.showConfirmationPopup = function (action, costEstimation, order) {
        var message = this.selectPopupMessageFromAction(action, costEstimation);
        return this.showPopup("confirmation", message, order);
    };
    MessageService.prototype.selectPopupMessageFromAction = function (action, costEstimation) {
        var title;
        var message;
        switch (action) {
            case 'cancel':
                title = "Do you really want to cancel the order?";
                message = "There will be a penalty fee of " + (costEstimation / 2).toFixed(2) + " EUR";
                break;
            case 'complete':
                title = 'Are you ready to complete the order?';
                message = 'Please confirm';
                break;
            case 'start':
                title = 'Are you ready to start?';
                message = 'Please confirm';
                break;
            case 'payment':
                title = "Are you ready to pay " + (costEstimation).toFixed(2) + " EUR?";
                message = "At the moment we accept only cash payments";
                break;
            case 'accept':
                title = "Are you ready to accept the order?";
                message = 'Please confirm';
                break;
            case 'terminate':
                title = "Do you really want to terminate the order?";
                message = 'Please confirm';
                break;
            case 'create':
                title = 'Are you ready to order a taxi?';
                message = "Estimated price: " + (costEstimation).toFixed(2) + " EUR";
                break;
        }
        this.logger.debug("MessageService: Selected popup message " + title + ", " + message + " from action " + action);
        return [title, message];
    };
    MessageService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialog"], ngx_logger__WEBPACK_IMPORTED_MODULE_4__["NGXLogger"]])
    ], MessageService);
    return MessageService;
}());



/***/ }),

/***/ "./src/app/services/order.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/order.service.ts ***!
  \*******************************************/
/*! exports provided: OrderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderService", function() { return OrderService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _models_order__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../models/order */ "./src/app/models/order.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _location_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./location.service */ "./src/app/services/location.service.ts");
/* harmony import */ var rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/BehaviorSubject */ "./node_modules/rxjs-compat/_esm5/BehaviorSubject.js");
/* harmony import */ var ngx_logger__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-logger */ "./node_modules/ngx-logger/esm5/ngx-logger.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");









var RATE_PER_KM = 0.5;
var RATE_RIDE_START = 2;
var OrderService = /** @class */ (function () {
    function OrderService(http, logger, locationService, router) {
        this.http = http;
        this.logger = logger;
        this.locationService = locationService;
        this.router = router;
        this.fixRate = 5;
        this.baseUrl = '/orders';
        this.newOrder = new _models_order__WEBPACK_IMPORTED_MODULE_3__["Order"]();
        this.lastOrder = new _models_order__WEBPACK_IMPORTED_MODULE_3__["Order"]();
        this.orderSource = new rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_6__["BehaviorSubject"](this.lastOrder);
        this.orderStatus = this.orderSource.asObservable();
        this.messageHistory = [];
        this.state = "NOT CONNECTED";
    }
    OrderService.prototype.add = function () {
        this.locationService.setCurrentPosition();
        this.newOrder.fromLat = this.locationService.coordinates.fromLat;
        this.newOrder.fromLong = this.locationService.coordinates.fromLong;
        this.newOrder.whereLat = this.locationService.coordinates.whereLat;
        this.newOrder.whereLong = this.locationService.coordinates.whereLong;
        this.newOrder.fromName = this.locationService.coordinates.fromName;
        this.newOrder.whereName = this.locationService.coordinates.whereName;
        this.newOrder.distance = this.locationService.calculateDistance(this.locationService.coordinates);
        this.newOrder.cost = this.calculateCost(this.newOrder.distance);
        this.logger.debug("OrderService: POST new order " + JSON.stringify(this.newOrder));
        return this.http.post(this.baseUrl + "/add", this.newOrder)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])((function (data) { return data; })));
    };
    OrderService.prototype.calculateCost = function (distance) {
        var cost = RATE_RIDE_START + RATE_PER_KM * distance * 2;
        this.logger.debug("OrderService: set last cost to: " + cost);
        return cost;
    };
    OrderService.prototype.setLastOrder = function (order) {
        this.logger.debug("OrderService: set last order to: " + JSON.stringify(order));
        this.orderSource.next(order);
    };
    OrderService.prototype.addRating = function (rating, order) {
        return this.http.post(this.baseUrl + "/" + order.orderId + "/feedback", rating)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])((function (data) { return data; })));
    };
    OrderService.prototype.getOpenOrders = function () {
        this.logger.debug("OrderService: requesting open orders");
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]().set('Content-Type', 'application/json');
        return this.http.get(this.baseUrl + "/open", { headers: headers }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) { return data; }));
    };
    OrderService.prototype.getOrdersHistory = function () {
        this.logger.debug("OrderService: requesting orders history");
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]().set('Content-Type', 'application/json');
        return this.http.get(this.baseUrl + "/history", { headers: headers }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) { return data; }));
    };
    OrderService.prototype.getActiveOrder = function () {
        var _this = this;
        this.logger.debug("OrderService: requesting active order for the user");
        var order = this.http.get(this.baseUrl + "/last-open").pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])((function (data) { return data; })));
        order.subscribe(function (order) {
            _this.logger.debug("OrderService: received active order: " + JSON.stringify(order) + ", updating orderSource");
            _this.orderSource.next(order);
        }, function (error) {
            _this.logger.debug("OrderService: error while receiving an active order: " + JSON.stringify(error));
            _this.router.navigate(['']);
        });
        return order;
    };
    OrderService.prototype.getOrderById = function (id) {
        this.logger.debug("OrderService: requesting open by id " + id);
        return this.http.get(this.baseUrl + "/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])((function (data) { return data; })));
    };
    OrderService.prototype.execute = function (action) {
        this.logger.debug("OrderService: executing action " + action);
        return this.http.get(this.baseUrl + "/" + action)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])((function (data) { return data; })));
    };
    OrderService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], ngx_logger__WEBPACK_IMPORTED_MODULE_7__["NGXLogger"], _location_service__WEBPACK_IMPORTED_MODULE_5__["LocationService"], _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"]])
    ], OrderService);
    return OrderService;
}());



/***/ }),

/***/ "./src/app/services/user.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/user.service.ts ***!
  \******************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _models_user__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../models/user */ "./src/app/models/user.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/BehaviorSubject */ "./node_modules/rxjs-compat/_esm5/BehaviorSubject.js");
/* harmony import */ var _menu_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./menu.service */ "./src/app/services/menu.service.ts");
/* harmony import */ var _web_socket_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./web-socket.service */ "./src/app/services/web-socket.service.ts");
/* harmony import */ var ngx_logger__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-logger */ "./node_modules/ngx-logger/esm5/ngx-logger.js");









var WEBSOCKET_URL = "ws://taxipy-backend.herokuapp.com/socket";
var EXAMPLE_URL = "/topic/server-broadcaster";
var UserService = /** @class */ (function () {
    function UserService(http, webSocketService, menuService, logger) {
        this.http = http;
        this.webSocketService = webSocketService;
        this.menuService = menuService;
        this.logger = logger;
        this.baseUrl = 'https://taxipy-backend.herokuapp.com/user';
        this.currentUser = new _models_user__WEBPACK_IMPORTED_MODULE_3__["User"]();
        this.authorized = false;
        this.authSource = new rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_5__["BehaviorSubject"](this.authorized);
        this.authStatus = this.authSource.asObservable();
        this.userSource = new rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_5__["BehaviorSubject"](this.currentUser);
        this.userStatus = this.userSource.asObservable();
    }
    UserService.prototype.getUser = function (id) {
        this.logger.debug("UserService: Requesting user by id: " + id);
        return this.http.get(this.baseUrl + "/" + id);
    };
    UserService.prototype.getProfilePicture = function () {
        return this.http.get(this.baseUrl + "/downloadDriverPhoto")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])((function (data) { return data; })));
    };
    UserService.prototype.reportLostItem = function (item) {
        var userType = null;
        if (this.currentUser.driver === null) {
            userType = 'lost';
        }
        else if (this.currentUser.passenger === null) {
            userType = 'found';
        }
        if (userType !== null) {
            return this.http.post("/" + userType + "/submit", item).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])((function (data) { return data; })));
        }
        else {
            this.logger.error("UserService: userType is not found");
        }
    };
    UserService.prototype.updateUserData = function (user) {
        this.authSource.next(true);
        this.userSource.next(user);
        this.currentUser = user;
        this.menuService.updateMenu();
        this.menuService.setCurrentUser(user);
        this.initiateBroadcasting();
        this.logger.debug("UserService: Authorizing user " + JSON.stringify(user));
        this.logger.debug("UserService: Updated menu, set current user");
    };
    UserService.prototype.registerUser = function (user) {
        this.logger.debug("UserService: Registering user " + JSON.stringify(user));
        return this.http.post(this.baseUrl + "/register", user).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])((function (data) { return data; })));
    };
    UserService.prototype.updateUser = function (user) {
        this.logger.debug("UserService: Updating the user " + JSON.stringify(user));
        return this.http.post(this.baseUrl + "/update", user);
    };
    UserService.prototype.getCurrentUser = function () {
        this.logger.debug("UserService: Getting user");
        return this.http.get(this.baseUrl + "/profile").pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])((function (data) { return data; })));
    };
    UserService.prototype.login = function (user) {
        this.logger.debug("UserService: Log in user " + JSON.stringify(user));
        var body = JSON.stringify({ username: user.username, password: user.password });
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]().set('Content-Type', 'application/json')
            .set('X-Requested-With', 'XMLHttpRequest');
        return this.http.post('https://taxipy-backend.herokuapp.com/auth', body, { headers: headers });
    };
    UserService.prototype.authorizeUser = function () {
        this.logger.debug("UserService: Authorizing user");
        var token = localStorage.getItem('token');
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]().set('Authorization', "Bearer " + token);
        console.log(token);
        return this.http.get('https://taxipy-backend.herokuapp.com/user', { headers: headers }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])((function (data) { return data; })));
    };
    UserService.prototype.initiateBroadcasting = function () {
        this.logger.debug("UserService: Initiating broadcasting via web socket");
        this.webSocketService.init(WEBSOCKET_URL, EXAMPLE_URL);
        this.menuService.monitorBroadcastMsg();
    };
    UserService.prototype.getUsersList = function () {
        this.logger.debug("UserService: Requesting user list");
        return this.http.get(this.baseUrl + "/list");
    };
    UserService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"],
            _web_socket_service__WEBPACK_IMPORTED_MODULE_7__["WebSocketService"],
            _menu_service__WEBPACK_IMPORTED_MODULE_6__["MenuService"],
            ngx_logger__WEBPACK_IMPORTED_MODULE_8__["NGXLogger"]])
    ], UserService);
    return UserService;
}());



/***/ }),

/***/ "./src/app/services/web-socket.service.ts":
/*!************************************************!*\
  !*** ./src/app/services/web-socket.service.ts ***!
  \************************************************/
/*! exports provided: WebSocketService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WebSocketService", function() { return WebSocketService; });
/* harmony import */ var _stomp_ng2_stompjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @stomp/ng2-stompjs */ "./node_modules/@stomp/ng2-stompjs/fesm5/stomp-ng2-stompjs.js");

var WebSocketService = /** @class */ (function () {
    function WebSocketService() {
    }
    WebSocketService.prototype.init = function (socketUrl, streamUrl) {
        var stompConfig = {
            url: socketUrl,
            headers: {
                login: "",
                passcode: ""
            },
            heartbeat_in: 0,
            heartbeat_out: 20000,
            reconnect_delay: 5000,
            debug: true
        };
        // Create Stomp Service
        this.stompService = new _stomp_ng2_stompjs__WEBPACK_IMPORTED_MODULE_0__["StompService"](stompConfig);
        // Connect to a Stream
        this.messages = this.stompService.subscribe(streamUrl);
    };
    WebSocketService.prototype.stream = function () {
        return this.messages;
    };
    WebSocketService.prototype.send = function (url, message) {
        return this.stompService.publish(url, JSON.stringify(message));
    };
    WebSocketService.prototype.state = function () {
        return this.stompService.state;
    };
    return WebSocketService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/nikolaikloga/Desktop/taxipy/front/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map