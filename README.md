# ![Juice Shop Logo](https://bitbucket.org/nkloga/taxipy-frontend/raw/c9929c4ff5da6ccb81fe61ad7ac1c56e79fc59ba/src/assets/images/favicon-32x32.png) Taxipy

Taxipy is a final project concluding a 9 month study in [Software Development Academy](https://sdacademy.ee/).
Taxi app project has been selected among others since it is challenging, complex enough and it will utilize technologies I've covered during the study.
Technologies in use:
- Java Spring
- Hibernate, MySql/H2 databases
- REST API
- Angular
- JWT token based auth
- Web sockets

## Functionality

The goal was to create a transportation app connecting taxi drivers with their potential clients. 

### Passenger:

- can order a taxi from current location to a specific destination (it should be within 30km)
- cancel the order, pay and rate the driver.
- report if something has been lost in the car
- modify own profile
- see orders history

### Driver:

- can see open orders, accept, start, terminate and complete the order
- can add own profile picture which will be displayed to the customers
- modify own profile
- see orders history

### Admin:

- can block / unblock any user
- can resolve lost/found cases
- can modify own profile

## Demo

Feel free to have a look at the latest version of Taxipy:
[Demo video](https://www.youtube.com/watch?v=49NEiPnsUKg)

## Setup

#### Deploy on Heroku (free ($0/month) dyno)

- You will have to deploy [Taxipy backend](https://bitbucket.org/nkloga/taxipy-backend/) and this project on separate heroku apps.
- Modify CORS URL in the backend
- Replace backend URL in the front-end project
- Request [Google Map API key](https://developers.google.com/maps/documentation/javascript/tutorial) and insert it into environment.ts
- [Sign up to Heroku](https://signup.heroku.com/) and
   [log in to your account](https://id.heroku.com/login)
- Click the button below and follow the instructions

[![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy)

> This is the quickest way to get a running instance of Taxipy! 

## Licensing 

This program is free software: you can redistribute it and/or modify it
under the terms of the [MIT license]().
